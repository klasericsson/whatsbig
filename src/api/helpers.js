import fetch from 'isomorphic-fetch';

const defaultHeaders = {
  'Content-Type': 'application/json',
  Accept: 'application/json',
};

const handleStatusErrors = (response) => {
  if (!response.ok) {
    const error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
  return response;
};

const handleErrorsAndParse = response =>
  response.json().then((json) => {
    if (!response.ok) {
      const content = json.errors ? json :
        Object.assign({}, json, { errors: [{ desc: response.statusText }] });
      return Promise.reject({ type: 'http', response, content });
    }
    return json;
  });

const handleContentErrors = (json) => {
  if (json.status === 'FAILURE') {
    return Promise.reject({ type: 'content', content: json });
  }
  return json;
};

const parseJSON = (response) => {
  return response.json();
};

export const makeRequest = (
  url, method = 'GET',
  headers = defaultHeaders, body, mode = 'cors'
) => fetch(url, {
  method,
  dataType: 'json',
  headers,
  // body,
  mode,
})
  .then(handleErrorsAndParse)
  .then(handleContentErrors);

// called from server side when accessing the private api, so no handleContentErrors
export const makePrivateRequest = (url, method = 'GET', body) =>
  fetch(url, {
    method,
    dataType: 'json',
    headers: defaultHeaders,
    body,
  })
  .then(handleStatusErrors)
  .then(parseJSON);
