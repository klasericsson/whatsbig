import { makeRequest } from './helpers';
import sampleData from './sample.json';

export default {
  getList: (countryCode, languageCode) => makeRequest('/json/BigList.json', 'GET', null, {'a': 'b'}),
  getListMore: (countryCode, languageCode, page) => makeRequest('json/BigList.json'),
  GetGlobalisationData: () => makeRequest('/json/GetGlobalizationdata.json'),
  sendFeedback: (payload) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(true);
      }, 2000);
    });
  },
  getItem: (alias) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({
          alias: sampleData.items[0].alias,
          data: sampleData.items[0],
        });
      }, 0);
    });
  },
};


//GetGlobalisationdata?language_code=en