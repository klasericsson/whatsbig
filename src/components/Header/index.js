import React, { PropTypes } from 'react';
import translate from 'redux-polyglot/translate';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import Menu from '../Menu';
import DropDown from '../DropDown';

// import LanguageSelect from '../containers/LanguageSelect';

const Header = ({ onSetMenuVisible, selectedLanguage, onLanguageChange, languages, p, menuVisible = false }) => {
  const languageOptions = languages.map(language => (
    {
      value: language.language_code,
      label: language.name,
    }
  ));

  return (
    <div className="site-header">
      <div className="site-header__container">
        <Link to={'/'} className="site-header__logo">Home</Link>
        <div className={classnames({ 'nav-wrapper': true, 'nav-wrapper--visible': menuVisible })}>
          {languages.length ?
            <div className="language-dropdown-wrapper">
              <DropDown
                className="dropdown--language"
                label={p.t('language_dropdown_title')}
                value={selectedLanguage}
                options={languageOptions}
                onChange={onLanguageChange}
              />
            </div> : null
          }
          <Menu />
        </div>
      </div>
      <button
        className={classnames({ 'site-header__menu-toggle': true, 'site-header__menu-toggle--close': menuVisible })}
        onClick={() => onSetMenuVisible(!menuVisible)}
      >
        MENU
      </button>
    </div>
  );
};

Header.propTypes = {
  onSetMenuVisible: PropTypes.func.isRequired,
  onLanguageChange: PropTypes.func.isRequired,
  selectedLanguage: PropTypes.string,
  languages: PropTypes.array,
  menuVisible: PropTypes.bool,
};

export default translate(Header);
