/* global __CONFIG__ */
import React, { PropTypes } from 'react';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import WhatsBigItem from '../../containers/WhatsBigItem';

const Packery = __CONFIG__.IS_CLIENT ? require('packery') : undefined;

// import LanguageSelect from '../containers/LanguageSelect';

class List extends React.Component {

  componentDidMount() {
    if (!this.props.noPackery) {
      this.grid = new Packery(this.container, {
        itemSelector: '.item',
        gutter: '.gutter-sizer',
        transitionDuration: 0,
      });
    }
  }

  shouldComponentUpdate(nextProps) {
    // if items.length && guid is different, update
    return true;
  }

  componentDidUpdate() {
    
    if (this.grid) {
      this.grid.reloadItems();
      this.grid.layout();
    }
  }

  render() {
    const { more, loadingMore, loading, items, onLoadMore } = this.props;

    return (
      <div className="list-wrapper">
        <div ref={(container) => { this.container = container; }} className="list">
          <div className="gutter-sizer" />
          {
            items.map((item, i) => (
              <WhatsBigItem key={i} item={item} theme={`theme-${(i % 9) + 1}`} />
            ))
          }
        </div>
        {
        more ?
          <button
            className={classnames({ 'flat-button': true, 'flat-button--loading': loadingMore })}
            onClick={() => {
              if (more && !loadingMore) {
                onLoadMore();
              }
            }}
          >
            <span>LOAD MORE</span>
            <div className="loader-small" />
          </button> : null
        }
      </div>
    );
  }

}

List.propTypes = {
  loading: PropTypes.bool,
  loadingMore: PropTypes.bool,
  more: PropTypes.bool,
  onLoadMore: PropTypes.func,
};

export default List;
