import React from 'react';
import classnames from 'classnames';

class DropDown extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  collapse() {
    this.setState({ open: false });
  }

  expand() {
    this.setState({ open: true });
  }

  handleOptionClick(value) {
    this.props.onChange(value);
  }

  render() {
    const { value, className, label = null, labelSuffix, options } = this.props;

    const selectedOption = options.filter(option => option.value === value);
    
    return (
      <div
        className={classnames({
          dropdown: true,
          'dropdown--open': this.state.open,
          [`${className}`]: className,
        })}
      >
        <button
          onBlur={() => this.collapse()}
          onClick={() => {
            if (this.state.open) {
              this.collapse();
            } else {
              this.expand();
            }
          }}
          className="dropdown__button"
        >
          {label || (selectedOption.length && selectedOption[0]).label}{labelSuffix || ''}
        </button>
        <div className="dropdown__options">
          {options.map(option => (
            <button
              key={option.value}
              className="dropdown__option"
              onMouseDown={() => this.handleOptionClick(option.value)}
              onTouchStart={() => this.handleOptionClick(option.value)}
            >
              {option.label}
            </button>
          ))}
        </div>
      </div>
    );
  }
}

export default DropDown;
