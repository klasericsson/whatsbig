/* global __CONFIG__, google */
import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import List from '../List';
import ShareButtons from '../../components/ShareButtons';


class MapView extends React.Component {

  componentDidMount() {
    this.markers = [];
    google.maps.Map.prototype.setCenterWithOffset = function (latlng, offsetX, offsetY) {
      const map = this;
      const ov = new google.maps.OverlayView();
      ov.onAdd = function () {
        const proj = this.getProjection();
        const aPoint = proj.fromLatLngToContainerPixel(latlng);
        aPoint.x += offsetX;
        aPoint.y += offsetY;
        map.panTo(proj.fromContainerPixelToLatLng(aPoint));
      };
      ov.draw = () => {};
      ov.setMap(this);
    };

    this.map = new google.maps.Map(this.mapEl, {
      center: { lat: 0, lng: 0 },
      zoom: 2,
      disableDefaultUI: true,
      zoomControl: true,
      zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER,
      },
    });
    this.offsetEl = document.querySelector('.map-list__header');
    this.createMarkers(this.map);
    this.setActiveCountry(this.props.selectedCountry);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedCountry !== this.props.selectedCountry) {
      this.setActiveCountry(nextProps.selectedCountry);
    }
  }

  setActiveCountry(country) {
    this.markers.forEach((marker) => {
      if (marker.country.country_code === country.country_code) {
        marker.setIcon(this.mapIcons.active);
        marker.active = true;
        this.map.setCenterWithOffset(
          marker.getPosition(),
          -this.offsetEl.getBoundingClientRect().width / 2,
          0,
        );
      } else {
        marker.setIcon(this.mapIcons.inactive);
        marker.active = false;
      }
    });
  }

  createMarkers(map) {
    this.mapIcons = {
      inactive: {
        url: '/images/wb-map-pin.png',
        anchor: new google.maps.Point(32, 32),
      },
      hover: {
        url: '/images/wb-map-pin-hover.png',
        anchor: new google.maps.Point(32, 32),
      },
      active: {
        url: '/images/wb-map-pin-active.png',
        anchor: new google.maps.Point(140, 96),
      },
    };

    this.props.countries.forEach((country) => {
      const marker = new google.maps.Marker({
        position: new google.maps.LatLng(country.lat, country.lng),
        map,
        icon: this.mapIcons.inactive,
        country,
      });

      google.maps.event.addListener(marker, 'mouseover', () => {
        if (!marker.active) {
          marker.setIcon(this.mapIcons.hover);
        }
      });
      google.maps.event.addListener(marker, 'mouseout', () => {
        if (!marker.active) {
          marker.setIcon(this.mapIcons.inactive);
        } else {
          marker.setIcon(this.mapIcons.active);
        }
      });
      google.maps.event.addListener(marker, 'click', () => {
        this.props.history.push(`/map/${marker.country.name}`);
      });
      this.markers.push(marker);
    });
  }

  render() {

    const { selectedCountry, countries } = this.props;

    const selectedCountryIndex = countries.indexOf(selectedCountry);

    const nextCountry = countries[selectedCountryIndex + 1] ? countries[selectedCountryIndex + 1] : countries[0];
    const prevCountry = selectedCountryIndex === 0 ? countries[countries.length - 1] : countries[selectedCountryIndex - 1];

    return (
      <div className="map">
        <div ref={(c) => { this.mapEl = c; }} className="map-container" />
        <div className="map-list">
          <div className="map-list__header">
            <h2>{this.props.title}</h2>
            <Link className="arrow-button arrow-button--prev" to={`/map/${prevCountry.name}`}>Previous</Link>
            <Link className="arrow-button arrow-button--next" to={`/map/${nextCountry.name}`}>Next</Link>
          </div>
          <div className="map-list__list">
            <List noPackery className="map-list" items={this.props.items} />
          </div>
          <div className="map-list__footer">
            <ShareButtons caption="Country share caption" url={window.location.href} />
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(MapView);
