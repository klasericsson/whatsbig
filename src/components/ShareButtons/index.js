import React from 'react';
import FacebookProvider, { Share } from 'react-facebook';

const ShareButtons = ({ url, caption }) => {
  const shareUrl = encodeURI(url);
  const twitterUrl = `https://twitter.com/intent/tweet?text=${encodeURIComponent(caption)}&url=${shareUrl}`;
  const googleUrl = `https://plus.google.com/share?url=${shareUrl}`;

  return (
    <div className="share">
      <div className="share-container">
        <a title="Share this" className="share-button" href={twitterUrl}><img height="32" width="32" alt="" src="/images/share-twitter.png" /></a>
        <FacebookProvider appID={__CONFIG__.FACEBOOK_APP_ID}>
          <Share href={url} quote={caption}>
            <button className="share-button" type="button"><img height="32" width="32" alt="" src="/images/share-facebook.png" /></button>
          </Share>
        </FacebookProvider>
        <a title="Share this" className="share-button" href={googleUrl}><img height="32" width="32" alt="" src="/images/share-google.png" /></a>
      </div>
    </div>
  );
};

export default ShareButtons;
