/* global __CONFIG__ */
import React, { PropTypes } from 'react';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import ShareButtons from '../ShareButtons';
import VideoItem from '../VideoItem';

const Flickity = __CONFIG__.IS_CLIENT ? require('flickity') : undefined;

const renderVideos = videos => (
  <div className="item__section">
    <div className="item__icon item__icon--video" />
    <div className="item__slider">
      {videos.map((video, i) => (
        <div className="item__content" key={i}>
          <VideoItem video={video} />
          <h3 className="item__heading">{video.title}</h3>
          <div className="item__description">
            {video.short_description}
          </div>
        </div>
      ))
      }
    </div>
  </div>
);

const renderImages = (images) => (
  <div className="item__section">
    <div className="item__icon item__icon--image" />
    <div className="item__slider">
      {images.map((image, i) => (
        <div className="item__content" key={i}>
          <div className="image-wrapper">
            <img src={image.url} alt="" />
          </div>
          <div className="item__description">
            {image.caption}
          </div>
        </div>
      ))
      }
    </div>
  </div>
);

const renderArticles = (articles) => (
  <div className="item__section">
    <div className="item__icon item__icon--article" />
    <div className="item__slider">
      {articles.map((article, i) => (
        <div className="item__content" key={i}>
          <div className="image-wrapper">
            <img src={article.thumbnail_url} alt="" />
          </div>
          <h3 className="item__heading">{article.title}</h3>
          <div className="item__description">
            {article.description}
            <p><a rel="noopener noreferrer" target="_blank" href={article.url}>Read more</a></p>
          </div>
        </div>
      ))
      }
    </div>
  </div>
);

class ListItem extends React.Component {

  componentDidMount() {
    const sections = this.container.querySelectorAll('.item__slider');
    Array.from(sections).forEach((section) => {
      if (section.querySelectorAll('.item__content').length > 1) {
        const flickity = new Flickity(section, {
          cellSelector: '.item__content',
          wrapAround: true,
          pageDots: true,
          prevNextButtons: false,
        });
      }
    });
  }

  render() {
    const { item, theme, enableSharing = true } = this.props;

    return (
      <article ref={(container) => { this.container = container; }} className={`item ${theme}`}>
        <div className="item__header">
          <h2 className="item__title"><Link to={`/${item.alias}`}>{item.title}</Link></h2>
        </div>
        {item.youtube_videos.length ? renderVideos(item.youtube_videos) : null}
        {item.images.length ? renderImages(item.images) : null}
        {item.content_pages.length ? renderArticles(item.content_pages) : null}
        <div className="item__footer">
          {enableSharing ?
            <ShareButtons url={`${__CONFIG__.BASE_URL}/${item.alias}`} caption={item.share_caption} />
            : null
          }
        </div>
      </article>
    );
  }
}

ListItem.propTypes = {
  theme: PropTypes.string,
  item: PropTypes.object,
  enableSharing: PropTypes.bool,
};

export default ListItem;
