import React from 'react';


class VideoItem extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      clicked: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState({ clicked: true });
  }

  render() {
    const { video } = this.props;
    const { clicked } = this.state;

    return (
      <div className="image-wrapper">
        {clicked ?
          <iframe src="https://www.youtube.com/embed/qREKP9oijWI?autoplay=1" frameBorder="0" allowFullScreen />
          :
          <div>
            <img src={video.thumbnail_url} alt="" />
            <button onClick={this.handleClick} className="play-button">Play</button>
          </div>
        }
      </div>
    );
  }
}

export default VideoItem;
