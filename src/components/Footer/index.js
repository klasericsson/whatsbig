import React from 'react';
import Modal from 'react-modal';
import translate from 'redux-polyglot/translate';
import Feedback from '../../containers/Feedback';

class Footer extends React.Component {

  render() {
    const { feedbackOpen } = this.state || { feedbackOpen: false };

    return (
      <div className="site-footer">
        <div className="site-footer__container">
          <button className="text-button" onClick={() => this.setState({ feedbackOpen: true })}>
            {this.props.p.t('send_feedback_title')}
          </button>
          <Modal
            isOpen={feedbackOpen}
            onRequestClose={() => this.setState({ feedbackOpen: false })}
            contentLabel=""
            overlayClassName="modal"
            className="modal__content"
          >
            <Feedback onRequestClose={() => this.setState({ feedbackOpen: false })} />
          </Modal>
          <div className="site-footer__copyright">&copy; Whatsbig {new Date().getFullYear()}</div>
        </div>
      </div>
    );
  }
}

export default translate(Footer);
