/* global __CONFIG__ */
import React, { PropTypes } from 'react';
import { compose } from 'redux';
import translate from 'redux-polyglot/translate';
import { Field, reduxForm } from 'redux-form';
import ReCAPTCHA from 'react-google-recaptcha';

const validate = (values) => {
  const errors = {};
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  if (!values.comment) {
    errors.comment = 'Required';
  }
  if (!values.captcharesponse) {
    errors.captcharesponse = 'Required';
  }

  return errors;
};

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => {
  const props = { ...input };
  return (
    <div className="field">
      <label htmlFor={props.name}>{label}</label>
      {type === 'textarea' ?
        <textarea {...input} name={props.name} />
        : <input {...input} type={type} />
      }
      <div className="field__error">
        {touched && ((error && <span>{error}</span>))}
      </div>
    </div>
  );
};

const Captcha = props => (
  <div className="captcha">
    <ReCAPTCHA
      sitekey={__CONFIG__.CAPTCHA_SITE_KEY}
      onChange={response => props.input.onChange(response)}
    />
    <div className="captcha__error">
      {props.meta.touched && props.meta.error}
    </div>
  </div>
);

const SuccessMessage = () => (
  <div>
    <h2>Thanks for your submission</h2>
    Lorem ipsum lorem ipsum
  </div>
);

const ErrorMessage = () => (
  <div>
    <h2>An error occured</h2>
    Lorem ipsum lorem ipsum
  </div>
);

const Message = ({ sent }) => {
  if (sent) {
    return <SuccessMessage />;
  }
  return <ErrorMessage />;
};

const FeedbackForm = ({ handleSubmit, sent, hasError, isSending, p }) => (
  <div className="feedback-form">
    <div className="feedback-form__header">
      <div className="feedback-form__description">
        { !hasError && !sent ?
          <div>
            <h2>{p.t('send_feedback_title')} {hasError}</h2>
            {p.t('send_feedback_text')}
          </div> : <Message sent={sent} />
        }
      </div>
    </div>
    { !hasError && !sent ?
      <form onSubmit={handleSubmit}>
        <Field name="name" type="text" component={renderField} label={p.t('form_name_label')} />
        <Field name="phone" type="text" component={renderField} label={p.t('form_phone_label')} />
        <Field name="email" type="email" component={renderField} label={`${p.t('form_email_label')}*`} />
        <Field name="comment" type="textarea" component={renderField} label={`${p.t('form_comment_label')}*`} />
        <div className="field">
          <Field name="captcharesponse" component={Captcha} />
        </div>
        { isSending ?
          <div className="flat-button"><div className="loader-small" />&nbsp;</div>
        :
          <button className="flat-button" type="submit">
            {p.t('form_ok_button_text')}
          </button>
        }
      </form>
      : null
    }
  </div>
);

FeedbackForm.propTypes = {
  sent: PropTypes.bool.isRequired,
  hasError: PropTypes.bool.isRequired,
  isSending: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
};

export default compose(
  translate,
  reduxForm({
    form: 'feedback',
    validate
  })
)(FeedbackForm);

// export default reduxForm({
//   form: 'feedback', validate
// })(FeedbackForm);
