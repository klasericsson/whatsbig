import React, { Component, PropTypes } from 'react';
import { compose } from 'redux';
import { NavLink, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import translate from 'redux-polyglot/translate';

const className = 'nav-button';
const activeClassName = 'nav-button--active';

const Menu = ({ p }) => (
  <nav>
    <div className={classnames({ 'site-navigation': true })}>
      <NavLink className={className} exact to={'/'} activeClassName={activeClassName}>
        <svg xmlns="http://www.w3.org/2000/svg" width="52.103" height="50.288" viewBox="0 0 52.103 50.288"><path fill="#fff" d="M0 0h14.77v14.77H0zM18.667 0h14.77v14.77h-14.77zM37.333 0h14.77v14.77h-14.77zM0 17.917h14.77v14.77H0zM18.667 17.917h14.77v14.77h-14.77zM37.333 17.917h14.77v14.77h-14.77zM0 35.52h14.77v14.768H0zM18.667 35.52h14.77v14.768h-14.77zM37.333 35.52h14.77v14.768h-14.77z" /></svg>
        {p.t('list_link_text')}
      </NavLink>
      <NavLink className={className} to={'/map'} activeClassName={activeClassName}>
        <svg xmlns="http://www.w3.org/2000/svg" width="20.469" height="47.778" viewBox="0 0 20.469 47.778"><circle fill="#E64D38" cx="10.235" cy="10.234" r="10.234"/><path fill="#E64D38" d="M13.37 44.21c.007-1.327-.713-2.486-1.784-3.11l.12-30.94-3.57-.014-.12 30.94c-1.075.615-1.804 1.768-1.81 3.096-.006 1.978 1.59 3.588 3.57 3.595 1.977.008 3.587-1.59 3.595-3.567z" /></svg>
        {p.t('map_link_text')}
      </NavLink>
    </div>
  </nav>
);

Menu.propTypes = {
  // visible: PropTypes.bool.isRequired,
};

export default compose(
  withRouter,
  translate
)(Menu);
