import Api from '../api';

export const SEND_FEEDBACK = 'SEND_FEEDBACK';
export const RESET_FEEDBACK = 'RESET_FEEDBACK';

export const sendFeedback = payload => ({
  type: SEND_FEEDBACK,
  promise: Api.sendFeedback(payload),
});

export const resetFeedback = () => ({
  type: RESET_FEEDBACK,
});
