import Api from '../api';

export const LOAD_LIST = 'LOAD_LIST';
export const LOAD_LIST_MORE = 'LOAD_LIST_MORE';

const logSuccess = () => {

};

export const loadList = (countryCode, languageCode) => ({
  type: LOAD_LIST,
  promise: Api.getList(countryCode, languageCode),
  meta: {
    onSuccess: response => logSuccess(response),
  },
});

export const loadListMore = (countryCode, languageCode, page) => ({
  type: LOAD_LIST_MORE,
  promise: Api.getListMore(countryCode, languageCode, page),
});
