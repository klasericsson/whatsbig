import Api from '../api';

export const LOAD_GLOBALISATION_DATA = 'SET_GLOBALISATION_DATA';

export const loadGlobalisationData = () => ({
  type: LOAD_GLOBALISATION_DATA,
  promise: Api.GetGlobalisationData(),
});
