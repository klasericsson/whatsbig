import Api from '../api';

export const LOAD_ITEM = 'LOAD_ITEM';

const logSuccess = () => {

};

export const loadItem = alias => ({
  type: LOAD_ITEM,
  promise: Api.getItem(alias),
  meta: {
    onSuccess: response => logSuccess(response),
  },
});
