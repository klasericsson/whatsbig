import { setLanguage as setLang } from 'redux-polyglot';

export const SET_MENU_VISIBLE = 'SET_MENU_VISIBLE';
export const SET_LANGUAGE = 'SET_LANGUAGE';
export const SET_COUNTRY = 'SET_COUNTRY';

const translations = require('../globalisation/translations.json');

export const setMenuVisible = visible => ({
  type: SET_MENU_VISIBLE,
  visible,
});

export const setLanguage = languageCode => (dispatch) => {
  dispatch({
    type: SET_LANGUAGE,
    languageCode,
  });
  dispatch(setLang(languageCode, translations[languageCode]));
};

export const setCountry = countryCode => ({
  type: SET_COUNTRY,
  countryCode,
});

// // import api from 'api';

// export const SET_LANGUAGE = 'SET_LANGUAGE';
// export const SET_COUNTRY = 'SET_COUNTRY';

// export const TEST_ACTION = 'TEST_ACTION';

// export const TEST_ASYNC_ACTION_START = 'TEST_ASYNC_ACTION_START';
// export const TEST_ASYNC_ACTION_ERROR = 'TEST_ASYNC_ACTION_ERROR';
// export const TEST_ASYNC_ACTION_SUCCESS = 'TEST_ASYNC_ACTION_SUCCESS';



// export function testAction() {
//   return {
//     type: TEST_ACTION,
//   };
// }

// // Async action example

// function testAsyncStart() {
//   return {
//     type: TEST_ASYNC_ACTION_START,
//   };
// }

// function testAsyncSuccess(data) {
//   return {
//     type: TEST_ASYNC_ACTION_SUCCESS,
//     data,
//   };
// }

// function testAsyncError(error) {
//   return {
//     type: TEST_ASYNC_ACTION_ERROR,
//     error,
//   };
// }

// export function testAsync() {
//   return function (dispatch) {
//     dispatch(testAsyncStart());

//     api.testAsync()
//       .then(data => dispatch(testAsyncSuccess(data)))
//       .catch(error => dispatch(testAsyncError(error)));
//   };
// }

// // Update
