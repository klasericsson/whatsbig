import { handle } from 'redux-pack';

import {
  LOAD_GLOBALISATION_DATA,
} from '../actions/globalisation';

const initialState = {
  isLoading: false,
  error: null,
  countries: [],
  languages: [],
};

const actionsMap = {
  [LOAD_GLOBALISATION_DATA]: (state, action) => handle(state, action, {
    start: s => ({ ...s, isLoading: true, error: null }),
    finish: s => ({ ...s, isLoading: false }),
    failure: s => ({ ...s, error: action.payload.error }),
    success: s => ({ ...s,
      countries: action.payload.countries,
      languages: action.payload.languages,
      more: action.payload.more,
    }),
    always: s => s,
  }),
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
