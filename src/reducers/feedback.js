import { handle } from 'redux-pack';

import {
  SEND_FEEDBACK,
  RESET_FEEDBACK,
} from '../actions/feedback';

const initialState = {
  isSending: false,
  error: false,
  sent: false,
};

const actionsMap = {
  [SEND_FEEDBACK]: (state, action) => handle(state, action, {
    start: s => ({ ...s, isSending: true, error: false, sent: false }),
    finish: s => ({ ...s, isSending: false }),
    failure: s => ({ ...s, error: true }),
    success: s => ({ ...s, error: false, sent: true }),
    always: s => s,
  }),
  [RESET_FEEDBACK]: () => initialState,
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
