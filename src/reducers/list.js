import { handle } from 'redux-pack';

import {
  LOAD_LIST,
  LOAD_LIST_MORE,
} from '../actions/list';

const initialState = {
  isLoading: false,
  isLoadingMore: false,
  error: null,
  guid: null,
  items: [],
  more: false,
  page: 0,
};

const actionsMap = {
  [LOAD_LIST]: (state, action) => handle(state, action, {
    start: s => ({ ...s, isLoading: true, error: null, items: [] }),
    finish: s => ({ ...s, isLoading: false }),
    failure: s => ({ ...s, error: action.payload.error }),
    success: s => ({ ...s, items: action.payload.items, more: action.payload.more, guid: action.payload.guid }),
    always: s => s,
  }),
  [LOAD_LIST_MORE]: (state, action) => handle(state, action, {
    start: s => ({ ...s, isLoadingMore: true, error: null }),
    finish: s => ({ ...s, isLoadingMore: false }),
    failure: s => ({ ...s, error: action.payload.error }),
    success: s => ({ ...s, items: [...state.items, ...action.payload.items], more: action.payload.more }),
    always: s => s,
  }),
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
