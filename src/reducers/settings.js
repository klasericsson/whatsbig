import {
  SET_MENU_VISIBLE,
  SET_COUNTRY,
  SET_LANGUAGE,
} from '../actions/settings';

const initialState = {
  languageCode: 'en',
  countryCode: 'GB',
  countryIndex: 0,
  menuVisible: false,
};

const actionsMap = {
  [SET_MENU_VISIBLE]: (state, action) => ({ ...state, menuVisible: action.visible }),
  [SET_COUNTRY]: (state, action) => ({ ...state, countryCode: action.countryCode }),
  [SET_LANGUAGE]: (state, action) => ({ ...state, languageCode: action.languageCode }),
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
