import { handle } from 'redux-pack';

import {
  LOAD_ITEM,
} from '../actions/item';

const initialState = {
  isLoading: false,
  error: null,
  data: null,
};

const actionsMap = {
  [LOAD_ITEM]: (state, action) => handle(state, action, {
    start: s => ({ ...s, isLoading: true, error: null, data: null }),
    finish: s => ({ ...s, isLoading: false }),
    failure: s => ({ ...s, error: action.payload.error }),
    success: s => ({ ...s, data: action.payload.data }),
    always: s => s,
  }),
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
