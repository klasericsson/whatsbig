import { combineReducers } from 'redux';
import { polyglotReducer as polyglot } from 'redux-polyglot';
import { reducer as form } from 'redux-form';

import settings from './settings';
import list from './list';
import feedback from './feedback';
import item from './item';
import globalisation from './globalisation';

export default combineReducers({
  globalisation,
  feedback,
  settings,
  list,
  item,
  polyglot,
  form,
});
