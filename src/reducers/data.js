import { handle } from 'redux-pack';

import {
  REQUEST_LIST,
} from 'actions/data';

const initialState = {
  list: [],
  languages: [],
  fetching: false,
};

const actionsMap = {
  [REQUEST_LIST]: (state, action) => handle(state, action, {
    start: s => ({
      ...s,
      isLoading: true,
      fooError: null
    }),
    finish: s => ({ ...s, isLoading: false }),
    failure: s => ({ ...s, fooError: action.payload }),
    success: s => ({ ...s, foo: action.payload }),
  }),
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}


// case ADD_ITEM :
//     return { 
//         ...state,
//         arr: [...state.arr, action.newItem]
//     }