import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import classnames from 'classnames';
import FeedbackForm from '../../components/FeedbackForm';
import { sendFeedback, resetFeedback } from '../../actions/feedback';

class Feedback extends Component {
  static propTypes = {
    onRequestClose: PropTypes.func.isRequired,
  };

  handleSubmit = (values) => {
    if (!this.props.sending) {
      this.props.sendFeedback(values);
    }
  }

  handleCloseModal = () => {
    this.props.resetFeedback();
    this.props.onRequestClose();
  }

  render() {
    const { isSending, sent, error } = this.props;
    return (
      <div
        className={classnames({
          'feedback-container': true,
          loading: true,
        })}
      >
        <FeedbackForm isSending={isSending} sent={sent} hasError={error} onSubmit={this.handleSubmit} />
        <button className="feedback-close" onClick={this.handleCloseModal}>Close</button>
      </div>
    );
  }

}

const mapStateToProps = ({ feedback }) => ({
  isSending: feedback.isSending,
  sent: feedback.sent,
  error: feedback.error,
});

Feedback.propTypes = {
  sendFeedback: PropTypes.func.isRequired,
  resetFeedback: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { sendFeedback, resetFeedback })(Feedback);
