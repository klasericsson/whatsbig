/* global __CONFIG__ */
import React, { Component, PropTypes } from 'react';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { Router, Route, Link, withRouter, Switch } from 'react-router-dom';
import { setLanguage } from '../../actions/settings';
import translate from 'redux-polyglot/translate';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import NotFound from '../../components/NotFound';

import WhatsBigList from '../../containers/WhatsBigList';
import WhatsBigMap from '../../containers/WhatsBigMap';
import Single from '../../containers/Single';
import Feedback from '../../containers/Feedback';
import { setMenuVisible } from '../../actions/settings';
import { loadGlobalisationData } from '../../actions/globalisation';
import { loadList } from '../../actions/list';

if (__CONFIG__.IS_CLIENT) {
  require('../../styles/main.scss');
}

class App extends Component {
  static propTypes = {
    setMenuVisible: PropTypes.func.isRequired,
    settings: PropTypes.object.isRequired,
  };

  componentDidMount() {
    this.props.setLanguage(this.props.settings.languageCode);
    this.props.loadGlobalisationData();
    this.props.loadList(this.props.settings.countryCode, this.props.settings.languageCode);
  }

  onLanguageChange(languageCode) {
    this.props.setLanguage(languageCode);
  }

  render() {
    const { settings, globalisation } = this.props;
    return (
      <div className="wrapper">
        <Header
          onLanguageChange={languageCode => this.onLanguageChange(languageCode)}
          menuVisible={settings.menuVisible}
          languages={globalisation.languages}
          onSetMenuVisible={this.props.setMenuVisible}
        />
        <main className="page">
          <Switch>
            <Route exact path="/" component={WhatsBigList} />
            <Route path="/map/:country" component={WhatsBigMap} />
            <Route path="/map" component={WhatsBigMap} />
            <Route path="/:id" component={Single} />
            <Route component={NotFound} />
          </Switch>
        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  settings: state.settings,
  globalisation: state.globalisation,
});
// function mapStateToProps(state) {
//   // const { transactions } = state;
//   // return {
//   //   transactions: transactions.transactions,
//   //   summary: transactions.summary,
//   //   gridFields: transactions.transactionsGrid
//   // };
//   return 0;
// }

//http://stackoverflow.com/questions/34836500/how-to-set-up-google-analytics-for-react-router

export default compose(
  withRouter,
  translate,
  connect(mapStateToProps, { setMenuVisible, setLanguage, loadList, loadGlobalisationData })
)(App);
