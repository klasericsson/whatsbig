import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Helmet } from 'react-helmet';
import translate from 'redux-polyglot/translate';
import DropDown from '../../components/DropDown';
import List from '../../components/List';
import { loadList, loadListMore } from '../../actions/list';
import { setCountry } from '../../actions/settings';

class WhatsBigList extends React.Component {

  constructor(props) {
    super(props);
    this.handleCountryChanged = this.handleCountryChanged.bind(this);
  }

  componentDidMount() {
    
  }

  handleCountryChanged(value) {
    this.props.onChangeCountry(value);
    this.props.onGetList(value, this.props.settings.languageCode);
  }

  render() {
    const { settings, items, more, loading, loadingMore, onLoadMore, globalisation } = this.props;
    const countryOptions = globalisation.countries.map(country => (
      {
        value: country.country_code,
        label: country.name,
      }
    ));

    const selectedCountry = globalisation.countries.filter(country => country.country_code === settings.countryCode);
    
    return (
      <div>
        <Helmet>
          <title>What's Big - List</title>
        </Helmet>
        {loading ? <div className="loader" /> : (
          <div className="list-page-wrapper">
            <div className="list-page-header">
              {globalisation.countries.length ?
                <div className="country-dropdown-wrapper">
                  <span>{this.props.p.t('currently_trending_text')}</span>
                  <DropDown
                    className="dropdown--country"
                    value={settings.countryCode}
                    label={selectedCountry.length ? selectedCountry[0].title_name : ''}
                    options={countryOptions}
                    onChange={this.handleCountryChanged}
                  />
                </div> : null
              }
            </div>
            <List more={more} loadingMore={loadingMore} onLoadMore={onLoadMore} items={items} />
          </div>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onLoadMore: (countryCode, languageCode, page) => {
      dispatch(loadListMore(countryCode, languageCode, page));
    },
    onGetList: (countryCode, languageCode) => {
      dispatch(loadList(countryCode, languageCode));
    },
    onChangeCountry: (countryCode) => {
      dispatch(setCountry(countryCode));
    }
  };
};

const mapStateToProps = state => (
  {
    globalisation: state.globalisation,
    items: state.list.items,
    loading: state.list.isLoading,
    loadingMore: state.list.isLoadingMore,
    more: state.list.more,
    guid: state.list.guid,
    settings: state.settings,
  }
);

export default compose(
  translate,
  connect(mapStateToProps, mapDispatchToProps)
)(WhatsBigList);
