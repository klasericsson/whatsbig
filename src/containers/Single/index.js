/* global __CONFIG__ */
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import FacebookProvider, { Comments } from 'react-facebook';
import classnames from 'classnames';
import { Helmet } from 'react-helmet';
import WhatsBigItem from '../WhatsBigItem';
import { loadItem } from '../../actions/item';

class Single extends Component {
  static propTypes = {
    summary: PropTypes.object,
    gridFields: PropTypes.array,
    actions: PropTypes.object
  };

  componentDidMount() {
    this.props.onGetItem(this.props.match.params.id);
  }

  render() {
    const { item } = this.props;
    const title = item.data ? item.data.title : '';

    return (
      <div
        className={classnames({
          single: true,
          loading: true,
        })}
      >
        <Helmet>
          <title>{`Whats Big - ${title}`}</title>
        </Helmet>
        {item.data ?
          <WhatsBigItem item={item.data} />
        : null
        }
        <div className="comments">
          {__CONFIG__.IS_CLIENT ?
            <FacebookProvider appID={__CONFIG__.FACEBOOK_APP_ID}>
              <Comments href={window.location.href} />
            </FacebookProvider> : null}
        </div>
      </div>
    );
  }

}

const mapDispatchToProps = (dispatch) => {
  return {
    onGetItem: (alias) => {
      dispatch(loadItem(alias));
    }
  };
};

const mapStateToProps = state => (
  {
    item: state.item,
    loading: state.item.isLoading,
    guid: state.item.guid,
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(Single);
