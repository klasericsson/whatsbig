/* global __CONFIG__ */
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import translate from 'redux-polyglot/translate';
import classnames from 'classnames';
import MapView from '../../components/MapView';
import { loadList } from '../../actions/list';
import { setCountry } from '../../actions/settings';

class WhatsBigMap extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      apiReady: false,
    };
    this.mapsApiReady = this.mapsApiReady.bind(this);
  }

  componentDidMount() {
    this.initMapsApi();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.globalisation.countries.length) {
      const countryFromUrl = this.props.globalisation.countries.filter(
        c => c.name.toLowerCase() === (nextProps.match.params.country || '').toLowerCase()
      )[0];
      const countryFromSettings = this.props.globalisation.countries.filter(
        c => c.country_code === this.props.settings.countryCode
      )[0];

      const country = countryFromUrl || countryFromSettings;
      if (country.country_code !== this.props.settings.countryCode) {
        this.props.setCountry(country.country_code);
        this.props.onGetList(country.country_code, this.props.settings.languageCode);
      }
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  initMapsApi() {
    if (window.google && window.google.maps) {
      this.mapsApiReady();
      return;
    }
    const global = `nd_const${Math.round(Math.random() * 99999)}`;
    window[global] = this.mapsApiReady;
    const script = document.createElement('script');
    script.onload = () => delete window[global];
    script.type = 'text/javascript';
    script.src = `https://maps.googleapis.com/maps/api/js?callback=${global}&key=${__CONFIG__.GOOGLE_MAPS_API_KEY}`;
    document.body.appendChild(script);
  }

  mapsApiReady() {
    this.setState({ apiReady: true });
  }

  render() {
    const { globalisation, settings } = this.props;
    const selectedCountry = globalisation.countries.filter(country => country.country_code === settings.countryCode);
    return (
      <div>
        <Helmet>
          <title>What's Big - Map</title>
        </Helmet>
        { this.state.apiReady && selectedCountry.length && globalisation.countries.length ?
          <MapView title={`${this.props.p.t('currently_trending_text')} ${selectedCountry[0].title_name}`} selectedCountry={selectedCountry[0]} countries={globalisation.countries} items={this.props.items} /> : <div className="loader" />
        }
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onGetList: (countryCode, languageCode) => {
      dispatch(loadList(countryCode, languageCode));
    },
    setCountry: (countryCode) => {
      dispatch(setCountry(countryCode));
    }
  };
};

const mapStateToProps = state => (
  {
    items: state.list.items,
    loading: state.list.isLoading,
    more: state.list.more,
    guid: state.list.guid,
    settings: state.settings,
    globalisation: state.globalisation,
  }
);

export default compose(
  translate,
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(WhatsBigMap);
