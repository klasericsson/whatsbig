/* global window, __CONFIG__ */
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { middleware as reduxPackMiddleware } from 'redux-pack';
import persistState from 'redux-localstorage';
import rootReducer from 'reducers';


export default function create(preloadedState) {
  const middleware = [thunk, reduxPackMiddleware];
  // const persistentState = persistState('settings', { key: 'wb' });

  let store;

  if (__CONFIG__.DEVELOPMENT && __CONFIG__.IS_CLIENT && __CONFIG__.ENABLE_DEVTOOLS) {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    store = createStore(
      rootReducer,
      preloadedState,
      composeEnhancers(
        applyMiddleware(...middleware),
        // persistentState,
      )
    );
  } else {
    store = createStore(
      rootReducer,
      preloadedState,
      applyMiddleware(...middleware),
      // persistentState,
    );
  }

  return store;
}
