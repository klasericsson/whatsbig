import Express from 'express';
import favicon from 'serve-favicon';
import compression from 'compression';
import path from 'path';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter, matchPath } from 'react-router-dom';
import { Provider } from 'react-redux';
import createStore from './store';
import App from './containers/App';
import renderPage from './index.html';

const PORT = process.env.PORT || 3000;

const app = new Express();
app.use(compression());
app.use(favicon(path.join(__dirname, '..', 'public', 'favicon.ico')));
app.use(Express.static(path.join(__dirname, '..', 'public')));

app.get('*', (req, res) => {
  // This context object contains the results of the render
  const context = {};

  // Create a new Redux store instance
  const store = createStore();

  const html = ReactDOMServer.renderToString(
    <Provider store={store}>
      <StaticRouter context={{}} location={req.url}>
        <App />
      </StaticRouter>
    </Provider>
  );

  // Grab the initial state from our Redux store
  const preloadedState = store.getState();

  // context.url will contain the URL to redirect to if a <Redirect> was used
  if (context.url) {
    res.writeHead(302, {
      Location: context.url
    });
    res.end();
  } else {
    res.write(renderPage(html, preloadedState));
    res.end();
  }
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
