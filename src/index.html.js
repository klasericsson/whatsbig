const html = (initialHtml, preloadedState) => (`
    <!doctype html>
    <html lang="en-US">
      <head>
        <meta charset="UTF-8">
        <title>Whatsbig</title>
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="stylesheet" href="/style.css">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </head>
      <body>
        <noscript>
          <style type="text/css">#root {display:none;}</style>
          <div class="no-script">Please enable javascript to use this site.</div>
        </noscript>
        <div id="root">${initialHtml}</div>
        <script>
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
        </script>
        <script src="/whatsbig.js"></script>
      </body>
    </html>
`);

export default html;
