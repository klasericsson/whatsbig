const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const path = require('path');

const srcPath = path.resolve(__dirname, 'src');
const distPath = path.resolve(__dirname, 'dist/server');
const envConfig = require('./environment.config');

const nodeEnv = process.env.NODE_ENV || "\"development\"";
const isProd = nodeEnv === 'production';

module.exports = {
  context: srcPath,
  entry: './server.js',
  output: {
    path: distPath,
    filename: 'server.js'
  },
  target: 'node',
  node: {
    __dirname: false,
    __filename: false
  },
  resolve: {
    modules: ['node_modules', 'src'],
    extensions: ['*', '.js', '.json']
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
    ]
  },
  externals: nodeExternals(),
  plugins: [
    new webpack.DefinePlugin({
      __CONFIG__: {
        ENVIRONMENT: nodeEnv,
        IS_CLIENT: false,
        IS_SERVER: true,
        DEVELOPMENT: isProd !== true,
        ENABLE_DEVTOOLS: false,
        API_URL: JSON.stringify(envConfig.apiUrl[nodeEnv]),
        BASE_URL: JSON.stringify(envConfig.baseUrl[nodeEnv]),
        COOKIE_NAME: JSON.stringify(envConfig.cookieName[nodeEnv]),
      },
    }),
  ]
  // devtool: 'source-map'
};
