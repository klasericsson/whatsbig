const webpack = require('webpack');
const path = require('path');
const envConfig = require('./environment.config');
const sourcePath = path.join(__dirname, './src');
const distPath = path.join(__dirname, './dist/public');
const ExtractTextPlugin = require('extract-text-webpack-plugin');


module.exports = function () {
  const nodeEnv = process.env.NODE_ENV || 'development';

  const isProd = nodeEnv === 'production';
  process.traceDeprecation = true;

  const plugins = [
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'vendor',
    //   minChunks: Infinity,
    //   filename: 'vendor.bundle.js'
    // }),
    new ExtractTextPlugin('style.css'),
    new webpack.EnvironmentPlugin({
      NODE_ENV: nodeEnv,
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
      __CONFIG__: {
        ENVIRONMENT: JSON.stringify(nodeEnv),
        IS_CLIENT: true,
        IS_SERVER: false,
        DEVELOPMENT: isProd !== true,
        ENABLE_DEVTOOLS: true,
        API_URL: JSON.stringify(envConfig.apiUrl[nodeEnv]),
        BASE_URL: JSON.stringify(envConfig.baseUrl[nodeEnv]),
        COOKIE_NAME: JSON.stringify(envConfig.cookieName[nodeEnv]),
        CAPTCHA_SITE_KEY: JSON.stringify(envConfig.captchaSiteKey[nodeEnv]),
        FACEBOOK_APP_ID: JSON.stringify(envConfig.facebookAppId),
        GOOGLE_MAPS_API_KEY: JSON.stringify(envConfig.googleMapsApiKey)
      },
    }),
  ];

  if (isProd) {
    plugins.push(
      new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false,
          screw_ie8: true,
          conditionals: true,
          unused: true,
          comparisons: true,
          sequences: true,
          dead_code: true,
          evaluate: true,
          if_return: true,
          join_vars: true,
        },
        output: {
          comments: false,
        },
      })
    );
  } else {
    plugins.push(
      // new webpack.HotModuleReplacementPlugin()
    );
  }

  return {
    devtool: isProd ? 'source-map' : 'eval',
    context: sourcePath,
    entry: {
      js: [
        './client.js'
      ],
      // vendor: ['react']
    },
    output: {
      path: distPath,
      filename: 'whatsbig.js',
    },
    module: {
      rules: [
        {
          test: /\.html$/,
          exclude: /node_modules/,
          use: {
            loader: 'file-loader',
            query: {
              name: '[name].[ext]'
            },
          },
        },
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            // resolve-url-loader may be chained before sass-loader if necessary
            use: [{ loader: 'css-loader', options: { minimize: true } }, 'sass-loader']
          })
        },
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: [
            'babel-loader'
          ],
        },
      ],
    },
    resolve: {
      extensions: ['.webpack-loader.js', '.web-loader.js', '.loader.js', '.js', '.jsx'],
      modules: [
        path.resolve(__dirname, 'node_modules'),
        sourcePath
      ]
    },

    plugins,

    performance: isProd && {
      maxAssetSize: 100,
      maxEntrypointSize: 300,
      hints: 'warning',
    },

    stats: {
      colors: {
        green: '\u001b[32m',
      }
    },

    devServer: {
      contentBase: './client',
      historyApiFallback: true,
      port: 3000,
      compress: isProd,
      inline: !isProd,
      hot: !isProd,
      stats: {
        assets: true,
        children: false,
        chunks: false,
        hash: false,
        modules: false,
        publicPath: false,
        timings: true,
        version: false,
        warnings: true,
        colors: {
          green: '\u001b[32m',
        }
      },
    }
  };
};
