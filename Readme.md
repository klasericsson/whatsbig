# WhatsBig

Frontend and Node.js web backend of WhatsBig

Make sure you have the latest node.js version. Some depenencies may fail in older versions. 

## Setup

```
$ npm install
```

## Running in dev mode

```
$ npm start build:dev
```

## Building in production mode

```
$ npm start build:dev
```

## Build (production)

```
$ npm start build:dev
```

## Starting the express web server
```
$ npm start server
```
