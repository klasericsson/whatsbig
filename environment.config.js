module.exports = {
  facebookAppId: '1459103764302324',
  googleMapsApiKey: 'AIzaSyAzuRa2i3B6tQ533znL-EPgS7ZYC6NqfS0',
  baseUrl: {
    development: 'http://localhost',
    staging: 'http://staging.whatsbig.com',
    production: 'http://whatsbig.com',
  },
  apiUrl: {
    development: 'localhost:api:dev',
    staging: 'localhost:api:staging',
    production: 'localhost:api:production',
  },
  cookieName: {
    development: 'WhatsBig.Settings.Staging.Dev',
    staging: 'WhatsBig.Settings.Staging',
    production: 'WhatsBig.Settings',
  },
  captchaSiteKey: {
    development: '6LdtLRoUAAAAAIFXYEMbep-4ABPN0duvpjm3iKxw',
    staging: '6LdtLRoUAAAAAIFXYEMbep-4ABPN0duvpjm3iKxw',
    production: '6LdtLRoUAAAAAIFXYEMbep-4ABPN0duvpjm3iKxw',
  },
};
