/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.l = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };

/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};

/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};

/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 55);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("react-router-dom");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("classnames");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("redux-polyglot/translate");

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux_polyglot__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux_polyglot___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_redux_polyglot__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return SET_MENU_VISIBLE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "f", function() { return SET_LANGUAGE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return SET_COUNTRY; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return setMenuVisible; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return setLanguage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return setCountry; });


var SET_MENU_VISIBLE = 'SET_MENU_VISIBLE';
var SET_LANGUAGE = 'SET_LANGUAGE';
var SET_COUNTRY = 'SET_COUNTRY';

var translations = __webpack_require__(49);

var setMenuVisible = function setMenuVisible(visible) {
  return {
    type: SET_MENU_VISIBLE,
    visible: visible
  };
};

var setLanguage = function setLanguage(languageCode) {
  return function (dispatch) {
    dispatch({
      type: SET_LANGUAGE,
      languageCode: languageCode
    });
    dispatch(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_redux_polyglot__["setLanguage"])(languageCode, translations[languageCode]));
  };
};

var setCountry = function setCountry(countryCode) {
  return {
    type: SET_COUNTRY,
    countryCode: countryCode
  };
};

// // import api from 'api';

// export const SET_LANGUAGE = 'SET_LANGUAGE';
// export const SET_COUNTRY = 'SET_COUNTRY';

// export const TEST_ACTION = 'TEST_ACTION';

// export const TEST_ASYNC_ACTION_START = 'TEST_ASYNC_ACTION_START';
// export const TEST_ASYNC_ACTION_ERROR = 'TEST_ASYNC_ACTION_ERROR';
// export const TEST_ASYNC_ACTION_SUCCESS = 'TEST_ASYNC_ACTION_SUCCESS';


// export function testAction() {
//   return {
//     type: TEST_ACTION,
//   };
// }

// // Async action example

// function testAsyncStart() {
//   return {
//     type: TEST_ASYNC_ACTION_START,
//   };
// }

// function testAsyncSuccess(data) {
//   return {
//     type: TEST_ASYNC_ACTION_SUCCESS,
//     data,
//   };
// }

// function testAsyncError(error) {
//   return {
//     type: TEST_ASYNC_ACTION_ERROR,
//     error,
//   };
// }

// export function testAsync() {
//   return function (dispatch) {
//     dispatch(testAsyncStart());

//     api.testAsync()
//       .then(data => dispatch(testAsyncSuccess(data)))
//       .catch(error => dispatch(testAsyncError(error)));
//   };
// }

// // Update

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("redux-pack");

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api__ = __webpack_require__(9);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return LOAD_LIST; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return LOAD_LIST_MORE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return loadListMore; });


var LOAD_LIST = 'LOAD_LIST';
var LOAD_LIST_MORE = 'LOAD_LIST_MORE';

var logSuccess = function logSuccess() {};

var loadList = function loadList(countryCode, languageCode) {
  return {
    type: LOAD_LIST,
    promise: __WEBPACK_IMPORTED_MODULE_0__api__["a" /* default */].getList(countryCode, languageCode),
    meta: {
      onSuccess: function onSuccess(response) {
        return logSuccess(response);
      }
    }
  };
};

var loadListMore = function loadListMore(countryCode, languageCode, page) {
  return {
    type: LOAD_LIST_MORE,
    promise: __WEBPACK_IMPORTED_MODULE_0__api__["a" /* default */].getListMore(countryCode, languageCode, page)
  };
};

/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__helpers__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sample_json__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__sample_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__sample_json__);



/* harmony default export */ __webpack_exports__["a"] = {
  getList: function getList(countryCode, languageCode) {
    return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__helpers__["a" /* makeRequest */])('/json/BigList.json', 'GET', null, { 'a': 'b' });
  },
  getListMore: function getListMore(countryCode, languageCode, page) {
    return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__helpers__["a" /* makeRequest */])('json/BigList.json');
  },
  GetGlobalisationData: function GetGlobalisationData() {
    return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__helpers__["a" /* makeRequest */])('/json/GetGlobalizationdata.json');
  },
  sendFeedback: function sendFeedback(payload) {
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        resolve(true);
      }, 2000);
    });
  },
  getItem: function getItem(alias) {
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        resolve({
          alias: __WEBPACK_IMPORTED_MODULE_1__sample_json___default.a.items[0].alias,
          data: __WEBPACK_IMPORTED_MODULE_1__sample_json___default.a.items[0]
        });
      }, 0);
    });
  }
};

//GetGlobalisationdata?language_code=en

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("react-helmet");

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api__ = __webpack_require__(9);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return SEND_FEEDBACK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return RESET_FEEDBACK; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return sendFeedback; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return resetFeedback; });


var SEND_FEEDBACK = 'SEND_FEEDBACK';
var RESET_FEEDBACK = 'RESET_FEEDBACK';

var sendFeedback = function sendFeedback(payload) {
  return {
    type: SEND_FEEDBACK,
    promise: __WEBPACK_IMPORTED_MODULE_0__api__["a" /* default */].sendFeedback(payload)
  };
};

var resetFeedback = function resetFeedback() {
  return {
    type: RESET_FEEDBACK
  };
};

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api__ = __webpack_require__(9);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LOAD_GLOBALISATION_DATA; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadGlobalisationData; });


var LOAD_GLOBALISATION_DATA = 'SET_GLOBALISATION_DATA';

var loadGlobalisationData = function loadGlobalisationData() {
  return {
    type: LOAD_GLOBALISATION_DATA,
    promise: __WEBPACK_IMPORTED_MODULE_0__api__["a" /* default */].GetGlobalisationData()
  };
};

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api__ = __webpack_require__(9);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return LOAD_ITEM; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loadItem; });


var LOAD_ITEM = 'LOAD_ITEM';

var logSuccess = function logSuccess() {};

var loadItem = function loadItem(alias) {
  return {
    type: LOAD_ITEM,
    promise: __WEBPACK_IMPORTED_MODULE_0__api__["a" /* default */].getItem(alias),
    meta: {
      onSuccess: function onSuccess(response) {
        return logSuccess(response);
      }
    }
  };
};

/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_classnames__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_classnames___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_classnames__);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var DropDown = function (_React$Component) {
  _inherits(DropDown, _React$Component);

  function DropDown(props) {
    _classCallCheck(this, DropDown);

    var _this = _possibleConstructorReturn(this, (DropDown.__proto__ || Object.getPrototypeOf(DropDown)).call(this, props));

    _this.state = {
      open: false
    };
    return _this;
  }

  _createClass(DropDown, [{
    key: 'componentDidMount',
    value: function componentDidMount() {}
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {}
  }, {
    key: 'collapse',
    value: function collapse() {
      this.setState({ open: false });
    }
  }, {
    key: 'expand',
    value: function expand() {
      this.setState({ open: true });
    }
  }, {
    key: 'handleOptionClick',
    value: function handleOptionClick(value) {
      this.props.onChange(value);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          value = _props.value,
          className = _props.className,
          _props$label = _props.label,
          label = _props$label === undefined ? null : _props$label,
          labelSuffix = _props.labelSuffix,
          options = _props.options;


      var selectedOption = options.filter(function (option) {
        return option.value === value;
      });

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        {
          className: __WEBPACK_IMPORTED_MODULE_1_classnames___default()(_defineProperty({
            dropdown: true,
            'dropdown--open': this.state.open
          }, '' + className, className))
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'button',
          {
            onBlur: function onBlur() {
              return _this2.collapse();
            },
            onClick: function onClick() {
              if (_this2.state.open) {
                _this2.collapse();
              } else {
                _this2.expand();
              }
            },
            className: 'dropdown__button'
          },
          label || (selectedOption.length && selectedOption[0]).label,
          labelSuffix || ''
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'dropdown__options' },
          options.map(function (option) {
            return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'button',
              {
                key: option.value,
                className: 'dropdown__option',
                onMouseDown: function onMouseDown() {
                  return _this2.handleOptionClick(option.value);
                },
                onTouchStart: function onTouchStart() {
                  return _this2.handleOptionClick(option.value);
                }
              },
              option.label
            );
          })
        )
      );
    }
  }]);

  return DropDown;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = DropDown;

/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_classnames__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_classnames___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_classnames__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_router_dom__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_router_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_router_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__containers_WhatsBigItem__ = __webpack_require__(18);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* global __CONFIG__ */





var Packery =  false ? require('packery') : undefined;

// import LanguageSelect from '../containers/LanguageSelect';

var List = function (_React$Component) {
  _inherits(List, _React$Component);

  function List() {
    _classCallCheck(this, List);

    return _possibleConstructorReturn(this, (List.__proto__ || Object.getPrototypeOf(List)).apply(this, arguments));
  }

  _createClass(List, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (!this.props.noPackery) {
        this.grid = new Packery(this.container, {
          itemSelector: '.item',
          gutter: '.gutter-sizer',
          transitionDuration: 0
        });
      }
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps) {
      // if items.length && guid is different, update
      return true;
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {

      if (this.grid) {
        this.grid.reloadItems();
        this.grid.layout();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          more = _props.more,
          loadingMore = _props.loadingMore,
          loading = _props.loading,
          items = _props.items,
          onLoadMore = _props.onLoadMore;


      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'list-wrapper' },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { ref: function ref(container) {
              _this2.container = container;
            }, className: 'list' },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'gutter-sizer' }),
          items.map(function (item, i) {
            return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__containers_WhatsBigItem__["a" /* default */], { key: i, item: item, theme: 'theme-' + (i % 9 + 1) });
          })
        ),
        more ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'button',
          {
            className: __WEBPACK_IMPORTED_MODULE_1_classnames___default()({ 'flat-button': true, 'flat-button--loading': loadingMore }),
            onClick: function onClick() {
              if (more && !loadingMore) {
                onLoadMore();
              }
            }
          },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'span',
            null,
            'LOAD MORE'
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'loader-small' })
        ) : null
      );
    }
  }]);

  return List;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

List.propTypes = {
  loading: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].bool,
  loadingMore: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].bool,
  more: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].bool,
  onLoadMore: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].func
};

/* harmony default export */ __webpack_exports__["a"] = List;

/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_facebook__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_facebook___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_facebook__);



var ShareButtons = function ShareButtons(_ref) {
  var url = _ref.url,
      caption = _ref.caption;

  var shareUrl = encodeURI(url);
  var twitterUrl = 'https://twitter.com/intent/tweet?text=' + encodeURIComponent(caption) + '&url=' + shareUrl;
  var googleUrl = 'https://plus.google.com/share?url=' + shareUrl;

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'div',
    { className: 'share' },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'share-container' },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'a',
        { title: 'Share this', className: 'share-button', href: twitterUrl },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { height: '32', width: '32', alt: '', src: '/images/share-twitter.png' })
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_1_react_facebook___default.a,
        { appID: __webpack_require__.i({"ENVIRONMENT":"development","IS_CLIENT":false,"IS_SERVER":true,"DEVELOPMENT":true,"ENABLE_DEVTOOLS":false,"API_URL":undefined,"BASE_URL":undefined,"COOKIE_NAME":undefined}).FACEBOOK_APP_ID },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_1_react_facebook__["Share"],
          { href: url, quote: caption },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'button',
            { className: 'share-button', type: 'button' },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { height: '32', width: '32', alt: '', src: '/images/share-facebook.png' })
          )
        )
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'a',
        { title: 'Share this', className: 'share-button', href: googleUrl },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { height: '32', width: '32', alt: '', src: '/images/share-google.png' })
      )
    )
  );
};

/* harmony default export */ __webpack_exports__["a"] = ShareButtons;

/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_classnames__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_classnames___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_classnames__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_FeedbackForm__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__actions_feedback__ = __webpack_require__(11);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }







var Feedback = function (_Component) {
  _inherits(Feedback, _Component);

  function Feedback() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Feedback);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Feedback.__proto__ || Object.getPrototypeOf(Feedback)).call.apply(_ref, [this].concat(args))), _this), _this.handleSubmit = function (values) {
      if (!_this.props.sending) {
        _this.props.sendFeedback(values);
      }
    }, _this.handleCloseModal = function () {
      _this.props.resetFeedback();
      _this.props.onRequestClose();
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Feedback, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          isSending = _props.isSending,
          sent = _props.sent,
          error = _props.error;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        {
          className: __WEBPACK_IMPORTED_MODULE_2_classnames___default()({
            'feedback-container': true,
            loading: true
          })
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__components_FeedbackForm__["a" /* default */], { isSending: isSending, sent: sent, hasError: error, onSubmit: this.handleSubmit }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'button',
          { className: 'feedback-close', onClick: this.handleCloseModal },
          'Close'
        )
      );
    }
  }]);

  return Feedback;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Feedback.propTypes = {
  onRequestClose: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].func.isRequired
};


var mapStateToProps = function mapStateToProps(_ref2) {
  var feedback = _ref2.feedback;
  return {
    isSending: feedback.isSending,
    sent: feedback.sent,
    error: feedback.error
  };
};

Feedback.propTypes = {
  sendFeedback: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].func.isRequired,
  resetFeedback: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].func.isRequired
};

/* harmony default export */ __webpack_exports__["a"] = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_react_redux__["connect"])(mapStateToProps, { sendFeedback: __WEBPACK_IMPORTED_MODULE_4__actions_feedback__["a" /* sendFeedback */], resetFeedback: __WEBPACK_IMPORTED_MODULE_4__actions_feedback__["b" /* resetFeedback */] })(Feedback);

/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_ListItem__ = __webpack_require__(34);


/* harmony default export */ __webpack_exports__["a"] = __WEBPACK_IMPORTED_MODULE_0__components_ListItem__["a" /* default */];

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = require("react-facebook");

/***/ }),
/* 20 */
/***/ (function(module, exports) {

module.exports = require("redux-form");

/***/ }),
/* 21 */
/***/ (function(module, exports) {

module.exports = require("redux-polyglot");

/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_router_dom__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_router_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_router_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__actions_settings__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_redux_polyglot_translate__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_redux_polyglot_translate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_redux_polyglot_translate__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_Header__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_Footer__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_NotFound__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__containers_WhatsBigList__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__containers_WhatsBigMap__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__containers_Single__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__containers_Feedback__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__actions_globalisation__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__actions_list__ = __webpack_require__(8);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* global __CONFIG__ */


















if (false) {
  require('../../styles/main.scss');
}

var App = function (_Component) {
  _inherits(App, _Component);

  function App() {
    _classCallCheck(this, App);

    return _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).apply(this, arguments));
  }

  _createClass(App, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.props.setLanguage(this.props.settings.languageCode);
      this.props.loadGlobalisationData();
      this.props.loadList(this.props.settings.countryCode, this.props.settings.languageCode);
    }
  }, {
    key: 'onLanguageChange',
    value: function onLanguageChange(languageCode) {
      this.props.setLanguage(languageCode);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          settings = _props.settings,
          globalisation = _props.globalisation;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'wrapper' },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__components_Header__["a" /* default */], {
          onLanguageChange: function onLanguageChange(languageCode) {
            return _this2.onLanguageChange(languageCode);
          },
          menuVisible: settings.menuVisible,
          languages: globalisation.languages,
          onSetMenuVisible: this.props.setMenuVisible
        }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'main',
          { className: 'page' },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_3_react_router_dom__["Switch"],
            null,
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_router_dom__["Route"], { exact: true, path: '/', component: __WEBPACK_IMPORTED_MODULE_9__containers_WhatsBigList__["a" /* default */] }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_router_dom__["Route"], { path: '/map/:country', component: __WEBPACK_IMPORTED_MODULE_10__containers_WhatsBigMap__["a" /* default */] }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_router_dom__["Route"], { path: '/map', component: __WEBPACK_IMPORTED_MODULE_10__containers_WhatsBigMap__["a" /* default */] }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_router_dom__["Route"], { path: '/:id', component: __WEBPACK_IMPORTED_MODULE_11__containers_Single__["a" /* default */] }),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_react_router_dom__["Route"], { component: __WEBPACK_IMPORTED_MODULE_8__components_NotFound__["a" /* default */] })
          )
        ),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_Footer__["a" /* default */], null)
      );
    }
  }]);

  return App;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

App.propTypes = {
  setMenuVisible: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].func.isRequired,
  settings: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].object.isRequired
};


var mapStateToProps = function mapStateToProps(state) {
  return {
    settings: state.settings,
    globalisation: state.globalisation
  };
};
// function mapStateToProps(state) {
//   // const { transactions } = state;
//   // return {
//   //   transactions: transactions.transactions,
//   //   summary: transactions.summary,
//   //   gridFields: transactions.transactionsGrid
//   // };
//   return 0;
// }

//http://stackoverflow.com/questions/34836500/how-to-set-up-google-analytics-for-react-router

/* harmony default export */ __webpack_exports__["a"] = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_redux__["compose"])(__WEBPACK_IMPORTED_MODULE_3_react_router_dom__["withRouter"], __WEBPACK_IMPORTED_MODULE_5_redux_polyglot_translate___default.a, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_react_redux__["connect"])(mapStateToProps, { setMenuVisible: __WEBPACK_IMPORTED_MODULE_4__actions_settings__["a" /* setMenuVisible */], setLanguage: __WEBPACK_IMPORTED_MODULE_4__actions_settings__["b" /* setLanguage */], loadList: __WEBPACK_IMPORTED_MODULE_14__actions_list__["a" /* loadList */], loadGlobalisationData: __WEBPACK_IMPORTED_MODULE_13__actions_globalisation__["a" /* loadGlobalisationData */] }))(App);

/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var html = function html(initialHtml, preloadedState) {
  return '\n    <!doctype html>\n    <html lang="en-US">\n      <head>\n        <meta charset="UTF-8">\n        <title>Whatsbig</title>\n        <link rel="shortcut icon" href="/favicon.ico" />\n        <link rel="stylesheet" href="/style.css">\n        <meta name="viewport" content="width=device-width, initial-scale=1" />\n      </head>\n      <body>\n        <noscript>\n          <style type="text/css">#root {display:none;}</style>\n          <div class="no-script">Please enable javascript to use this site.</div>\n        </noscript>\n        <div id="root">' + initialHtml + '</div>\n        <script>\n          window.__PRELOADED_STATE__ = ' + JSON.stringify(preloadedState).replace(/</g, '\\u003c') + '\n        </script>\n        <script src="/whatsbig.js"></script>\n      </body>\n    </html>\n';
};

/* harmony default export */ __webpack_exports__["a"] = html;

/***/ }),
/* 24 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux_thunk__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux_thunk___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_redux_thunk__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux_pack__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux_pack___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_redux_pack__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux_localstorage__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux_localstorage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_redux_localstorage__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_reducers__ = __webpack_require__(44);
/* harmony export (immutable) */ __webpack_exports__["a"] = create;
/* global window, __CONFIG__ */






function create(preloadedState) {
  var middleware = [__WEBPACK_IMPORTED_MODULE_1_redux_thunk___default.a, __WEBPACK_IMPORTED_MODULE_2_redux_pack__["middleware"]];
  // const persistentState = persistState('settings', { key: 'wb' });

  var store = void 0;

  if (false) {
    var composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    store = createStore(rootReducer, preloadedState, composeEnhancers(applyMiddleware.apply(undefined, middleware)));
  } else {
    store = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_redux__["createStore"])(__WEBPACK_IMPORTED_MODULE_4_reducers__["a" /* default */], preloadedState, __WEBPACK_IMPORTED_MODULE_0_redux__["applyMiddleware"].apply(undefined, middleware));
  }

  return store;
}

/***/ }),
/* 25 */
/***/ (function(module, exports) {

module.exports = require("compression");

/***/ }),
/* 26 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 27 */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),
/* 28 */
/***/ (function(module, exports) {

module.exports = require("react-dom/server");

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = require("serve-favicon");

/***/ }),
/* 30 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_isomorphic_fetch__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_isomorphic_fetch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_isomorphic_fetch__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return makeRequest; });
/* unused harmony export makePrivateRequest */


var defaultHeaders = {
  'Content-Type': 'application/json',
  Accept: 'application/json'
};

var handleStatusErrors = function handleStatusErrors(response) {
  if (!response.ok) {
    var error = new Error(response.statusText);
    error.response = response;
    throw error;
  }
  return response;
};

var handleErrorsAndParse = function handleErrorsAndParse(response) {
  return response.json().then(function (json) {
    if (!response.ok) {
      var content = json.errors ? json : Object.assign({}, json, { errors: [{ desc: response.statusText }] });
      return Promise.reject({ type: 'http', response: response, content: content });
    }
    return json;
  });
};

var handleContentErrors = function handleContentErrors(json) {
  if (json.status === 'FAILURE') {
    return Promise.reject({ type: 'content', content: json });
  }
  return json;
};

var parseJSON = function parseJSON(response) {
  return response.json();
};

var makeRequest = function makeRequest(url) {
  var method = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'GET';
  var headers = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : defaultHeaders;
  var body = arguments[3];
  var mode = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'cors';
  return __WEBPACK_IMPORTED_MODULE_0_isomorphic_fetch___default()(url, {
    method: method,
    dataType: 'json',
    headers: headers,
    // body,
    mode: mode
  }).then(handleErrorsAndParse).then(handleContentErrors);
};

// called from server side when accessing the private api, so no handleContentErrors
var makePrivateRequest = function makePrivateRequest(url) {
  var method = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'GET';
  var body = arguments[2];
  return __WEBPACK_IMPORTED_MODULE_0_isomorphic_fetch___default()(url, {
    method: method,
    dataType: 'json',
    headers: defaultHeaders,
    body: body
  }).then(handleStatusErrors).then(parseJSON);
};

/***/ }),
/* 31 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux_polyglot_translate__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux_polyglot_translate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_redux_polyglot_translate__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux_form__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux_form___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_redux_form__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react_google_recaptcha__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react_google_recaptcha___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_react_google_recaptcha__);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

/* global __CONFIG__ */






var validate = function validate(values) {
  var errors = {};
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  if (!values.comment) {
    errors.comment = 'Required';
  }
  if (!values.captcharesponse) {
    errors.captcharesponse = 'Required';
  }

  return errors;
};

var renderField = function renderField(_ref) {
  var input = _ref.input,
      label = _ref.label,
      type = _ref.type,
      _ref$meta = _ref.meta,
      touched = _ref$meta.touched,
      error = _ref$meta.error,
      warning = _ref$meta.warning;

  var props = _extends({}, input);
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'div',
    { className: 'field' },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'label',
      { htmlFor: props.name },
      label
    ),
    type === 'textarea' ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('textarea', _extends({}, input, { name: props.name })) : __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('input', _extends({}, input, { type: type })),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'field__error' },
      touched && error && __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'span',
        null,
        error
      )
    )
  );
};

var Captcha = function Captcha(props) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'div',
    { className: 'captcha' },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4_react_google_recaptcha___default.a, {
      sitekey: __webpack_require__.i({"ENVIRONMENT":"development","IS_CLIENT":false,"IS_SERVER":true,"DEVELOPMENT":true,"ENABLE_DEVTOOLS":false,"API_URL":undefined,"BASE_URL":undefined,"COOKIE_NAME":undefined}).CAPTCHA_SITE_KEY,
      onChange: function onChange(response) {
        return props.input.onChange(response);
      }
    }),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'captcha__error' },
      props.meta.touched && props.meta.error
    )
  );
};

var SuccessMessage = function SuccessMessage() {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'div',
    null,
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'h2',
      null,
      'Thanks for your submission'
    ),
    'Lorem ipsum lorem ipsum'
  );
};

var ErrorMessage = function ErrorMessage() {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'div',
    null,
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'h2',
      null,
      'An error occured'
    ),
    'Lorem ipsum lorem ipsum'
  );
};

var Message = function Message(_ref2) {
  var sent = _ref2.sent;

  if (sent) {
    return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(SuccessMessage, null);
  }
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(ErrorMessage, null);
};

var FeedbackForm = function FeedbackForm(_ref3) {
  var handleSubmit = _ref3.handleSubmit,
      sent = _ref3.sent,
      hasError = _ref3.hasError,
      isSending = _ref3.isSending,
      p = _ref3.p;
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'div',
    { className: 'feedback-form' },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'feedback-form__header' },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'feedback-form__description' },
        !hasError && !sent ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          null,
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'h2',
            null,
            p.t('send_feedback_title'),
            ' ',
            hasError
          ),
          p.t('send_feedback_text')
        ) : __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(Message, { sent: sent })
      )
    ),
    !hasError && !sent ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'form',
      { onSubmit: handleSubmit },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_redux_form__["Field"], { name: 'name', type: 'text', component: renderField, label: p.t('form_name_label') }),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_redux_form__["Field"], { name: 'phone', type: 'text', component: renderField, label: p.t('form_phone_label') }),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_redux_form__["Field"], { name: 'email', type: 'email', component: renderField, label: p.t('form_email_label') + '*' }),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_redux_form__["Field"], { name: 'comment', type: 'textarea', component: renderField, label: p.t('form_comment_label') + '*' }),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'field' },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3_redux_form__["Field"], { name: 'captcharesponse', component: Captcha })
      ),
      isSending ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'flat-button' },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'loader-small' }),
        '\xA0'
      ) : __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'button',
        { className: 'flat-button', type: 'submit' },
        p.t('form_ok_button_text')
      )
    ) : null
  );
};

FeedbackForm.propTypes = {
  sent: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].bool.isRequired,
  hasError: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].bool.isRequired,
  isSending: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].bool.isRequired,
  handleSubmit: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].func.isRequired
};

/* harmony default export */ __webpack_exports__["a"] = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_redux__["compose"])(__WEBPACK_IMPORTED_MODULE_2_redux_polyglot_translate___default.a, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3_redux_form__["reduxForm"])({
  form: 'feedback',
  validate: validate
}))(FeedbackForm);

// export default reduxForm({
//   form: 'feedback', validate
// })(FeedbackForm);

/***/ }),
/* 32 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_modal__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_modal__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux_polyglot_translate__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux_polyglot_translate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_redux_polyglot_translate__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__containers_Feedback__ = __webpack_require__(17);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }






var Footer = function (_React$Component) {
  _inherits(Footer, _React$Component);

  function Footer() {
    _classCallCheck(this, Footer);

    return _possibleConstructorReturn(this, (Footer.__proto__ || Object.getPrototypeOf(Footer)).apply(this, arguments));
  }

  _createClass(Footer, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _ref = this.state || { feedbackOpen: false },
          feedbackOpen = _ref.feedbackOpen;

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'site-footer' },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'site-footer__container' },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'button',
            { className: 'text-button', onClick: function onClick() {
                return _this2.setState({ feedbackOpen: true });
              } },
            this.props.p.t('send_feedback_title')
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            __WEBPACK_IMPORTED_MODULE_1_react_modal___default.a,
            {
              isOpen: feedbackOpen,
              onRequestClose: function onRequestClose() {
                return _this2.setState({ feedbackOpen: false });
              },
              contentLabel: '',
              overlayClassName: 'modal',
              className: 'modal__content'
            },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__containers_Feedback__["a" /* default */], { onRequestClose: function onRequestClose() {
                return _this2.setState({ feedbackOpen: false });
              } })
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'site-footer__copyright' },
            '\xA9 Whatsbig ',
            new Date().getFullYear()
          )
        )
      );
    }
  }]);

  return Footer;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = __WEBPACK_IMPORTED_MODULE_2_redux_polyglot_translate___default()(Footer);

/***/ }),
/* 33 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux_polyglot_translate__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux_polyglot_translate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_redux_polyglot_translate__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_router_dom__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_router_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_router_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_classnames__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_classnames___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_classnames__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Menu__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__DropDown__ = __webpack_require__(14);







// import LanguageSelect from '../containers/LanguageSelect';

var Header = function Header(_ref) {
  var onSetMenuVisible = _ref.onSetMenuVisible,
      selectedLanguage = _ref.selectedLanguage,
      onLanguageChange = _ref.onLanguageChange,
      languages = _ref.languages,
      p = _ref.p,
      _ref$menuVisible = _ref.menuVisible,
      menuVisible = _ref$menuVisible === undefined ? false : _ref$menuVisible;

  var languageOptions = languages.map(function (language) {
    return {
      value: language.language_code,
      label: language.name
    };
  });

  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'div',
    { className: 'site-header' },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'site-header__container' },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_2_react_router_dom__["Link"],
        { to: '/', className: 'site-header__logo' },
        'Home'
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: __WEBPACK_IMPORTED_MODULE_3_classnames___default()({ 'nav-wrapper': true, 'nav-wrapper--visible': menuVisible }) },
        languages.length ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'language-dropdown-wrapper' },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__DropDown__["a" /* default */], {
            className: 'dropdown--language',
            label: p.t('language_dropdown_title'),
            value: selectedLanguage,
            options: languageOptions,
            onChange: onLanguageChange
          })
        ) : null,
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__Menu__["a" /* default */], null)
      )
    ),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'button',
      {
        className: __WEBPACK_IMPORTED_MODULE_3_classnames___default()({ 'site-header__menu-toggle': true, 'site-header__menu-toggle--close': menuVisible }),
        onClick: function onClick() {
          return onSetMenuVisible(!menuVisible);
        }
      },
      'MENU'
    )
  );
};

Header.propTypes = {
  onSetMenuVisible: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].func.isRequired,
  onLanguageChange: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].func.isRequired,
  selectedLanguage: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].string,
  languages: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].array,
  menuVisible: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].bool
};

/* harmony default export */ __webpack_exports__["a"] = __WEBPACK_IMPORTED_MODULE_1_redux_polyglot_translate___default()(Header);

/***/ }),
/* 34 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_router_dom__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_router_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_router_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_classnames__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_classnames___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_classnames__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ShareButtons__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__VideoItem__ = __webpack_require__(38);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* global __CONFIG__ */






var Flickity =  false ? require('flickity') : undefined;

var renderVideos = function renderVideos(videos) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'div',
    { className: 'item__section' },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'item__icon item__icon--video' }),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'item__slider' },
      videos.map(function (video, i) {
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'item__content', key: i },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__VideoItem__["a" /* default */], { video: video }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'h3',
            { className: 'item__heading' },
            video.title
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'item__description' },
            video.short_description
          )
        );
      })
    )
  );
};

var renderImages = function renderImages(images) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'div',
    { className: 'item__section' },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'item__icon item__icon--image' }),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'item__slider' },
      images.map(function (image, i) {
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'item__content', key: i },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'image-wrapper' },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { src: image.url, alt: '' })
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'item__description' },
            image.caption
          )
        );
      })
    )
  );
};

var renderArticles = function renderArticles(articles) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'div',
    { className: 'item__section' },
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'item__icon item__icon--article' }),
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: 'item__slider' },
      articles.map(function (article, i) {
        return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'item__content', key: i },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'image-wrapper' },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('img', { src: article.thumbnail_url, alt: '' })
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'h3',
            { className: 'item__heading' },
            article.title
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'item__description' },
            article.description,
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'p',
              null,
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'a',
                { rel: 'noopener noreferrer', target: '_blank', href: article.url },
                'Read more'
              )
            )
          )
        );
      })
    )
  );
};

var ListItem = function (_React$Component) {
  _inherits(ListItem, _React$Component);

  function ListItem() {
    _classCallCheck(this, ListItem);

    return _possibleConstructorReturn(this, (ListItem.__proto__ || Object.getPrototypeOf(ListItem)).apply(this, arguments));
  }

  _createClass(ListItem, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var sections = this.container.querySelectorAll('.item__slider');
      Array.from(sections).forEach(function (section) {
        if (section.querySelectorAll('.item__content').length > 1) {
          var flickity = new Flickity(section, {
            cellSelector: '.item__content',
            wrapAround: true,
            pageDots: true,
            prevNextButtons: false
          });
        }
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          item = _props.item,
          theme = _props.theme,
          _props$enableSharing = _props.enableSharing,
          enableSharing = _props$enableSharing === undefined ? true : _props$enableSharing;


      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'article',
        { ref: function ref(container) {
            _this2.container = container;
          }, className: 'item ' + theme },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'item__header' },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'h2',
            { className: 'item__title' },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_1_react_router_dom__["Link"],
              { to: '/' + item.alias },
              item.title
            )
          )
        ),
        item.youtube_videos.length ? renderVideos(item.youtube_videos) : null,
        item.images.length ? renderImages(item.images) : null,
        item.content_pages.length ? renderArticles(item.content_pages) : null,
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'item__footer' },
          enableSharing ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__ShareButtons__["a" /* default */], { url: undefined + '/' + item.alias, caption: item.share_caption }) : null
        )
      );
    }
  }]);

  return ListItem;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

ListItem.propTypes = {
  theme: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].string,
  item: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].object,
  enableSharing: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].bool
};

/* harmony default export */ __webpack_exports__["a"] = ListItem;

/***/ }),
/* 35 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_router_dom__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_router_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_router_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__List__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_ShareButtons__ = __webpack_require__(16);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* global __CONFIG__, google */





var MapView = function (_React$Component) {
  _inherits(MapView, _React$Component);

  function MapView() {
    _classCallCheck(this, MapView);

    return _possibleConstructorReturn(this, (MapView.__proto__ || Object.getPrototypeOf(MapView)).apply(this, arguments));
  }

  _createClass(MapView, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.markers = [];
      google.maps.Map.prototype.setCenterWithOffset = function (latlng, offsetX, offsetY) {
        var map = this;
        var ov = new google.maps.OverlayView();
        ov.onAdd = function () {
          var proj = this.getProjection();
          var aPoint = proj.fromLatLngToContainerPixel(latlng);
          aPoint.x += offsetX;
          aPoint.y += offsetY;
          map.panTo(proj.fromContainerPixelToLatLng(aPoint));
        };
        ov.draw = function () {};
        ov.setMap(this);
      };

      this.map = new google.maps.Map(this.mapEl, {
        center: { lat: 0, lng: 0 },
        zoom: 2,
        disableDefaultUI: true,
        zoomControl: true,
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_CENTER
        }
      });
      this.offsetEl = document.querySelector('.map-list__header');
      this.createMarkers(this.map);
      this.setActiveCountry(this.props.selectedCountry);
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.selectedCountry !== this.props.selectedCountry) {
        this.setActiveCountry(nextProps.selectedCountry);
      }
    }
  }, {
    key: 'setActiveCountry',
    value: function setActiveCountry(country) {
      var _this2 = this;

      this.markers.forEach(function (marker) {
        if (marker.country.country_code === country.country_code) {
          marker.setIcon(_this2.mapIcons.active);
          marker.active = true;
          _this2.map.setCenterWithOffset(marker.getPosition(), -_this2.offsetEl.getBoundingClientRect().width / 2, 0);
        } else {
          marker.setIcon(_this2.mapIcons.inactive);
          marker.active = false;
        }
      });
    }
  }, {
    key: 'createMarkers',
    value: function createMarkers(map) {
      var _this3 = this;

      this.mapIcons = {
        inactive: {
          url: '/images/wb-map-pin.png',
          anchor: new google.maps.Point(32, 32)
        },
        hover: {
          url: '/images/wb-map-pin-hover.png',
          anchor: new google.maps.Point(32, 32)
        },
        active: {
          url: '/images/wb-map-pin-active.png',
          anchor: new google.maps.Point(140, 96)
        }
      };

      this.props.countries.forEach(function (country) {
        var marker = new google.maps.Marker({
          position: new google.maps.LatLng(country.lat, country.lng),
          map: map,
          icon: _this3.mapIcons.inactive,
          country: country
        });

        google.maps.event.addListener(marker, 'mouseover', function () {
          if (!marker.active) {
            marker.setIcon(_this3.mapIcons.hover);
          }
        });
        google.maps.event.addListener(marker, 'mouseout', function () {
          if (!marker.active) {
            marker.setIcon(_this3.mapIcons.inactive);
          } else {
            marker.setIcon(_this3.mapIcons.active);
          }
        });
        google.maps.event.addListener(marker, 'click', function () {
          _this3.props.history.push('/map/' + marker.country.name);
        });
        _this3.markers.push(marker);
      });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      var _props = this.props,
          selectedCountry = _props.selectedCountry,
          countries = _props.countries;


      var selectedCountryIndex = countries.indexOf(selectedCountry);

      var nextCountry = countries[selectedCountryIndex + 1] ? countries[selectedCountryIndex + 1] : countries[0];
      var prevCountry = selectedCountryIndex === 0 ? countries[countries.length - 1] : countries[selectedCountryIndex - 1];

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        { className: 'map' },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { ref: function ref(c) {
            _this4.mapEl = c;
          }, className: 'map-container' }),
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'map-list' },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'map-list__header' },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'h2',
              null,
              this.props.title
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_1_react_router_dom__["Link"],
              { className: 'arrow-button arrow-button--prev', to: '/map/' + prevCountry.name },
              'Previous'
            ),
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              __WEBPACK_IMPORTED_MODULE_1_react_router_dom__["Link"],
              { className: 'arrow-button arrow-button--next', to: '/map/' + nextCountry.name },
              'Next'
            )
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'map-list__list' },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__List__["a" /* default */], { noPackery: true, className: 'map-list', items: this.props.items })
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'map-list__footer' },
            __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_3__components_ShareButtons__["a" /* default */], { caption: 'Country share caption', url: window.location.href })
          )
        )
      );
    }
  }]);

  return MapView;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_react_router_dom__["withRouter"])(MapView);

/***/ }),
/* 36 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_router_dom__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_router_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_router_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_classnames__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_classnames___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_classnames__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_polyglot_translate__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_polyglot_translate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_redux_polyglot_translate__);






var className = 'nav-button';
var activeClassName = 'nav-button--active';

var Menu = function Menu(_ref) {
  var p = _ref.p;
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'nav',
    null,
    __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
      'div',
      { className: __WEBPACK_IMPORTED_MODULE_3_classnames___default()({ 'site-navigation': true }) },
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_2_react_router_dom__["NavLink"],
        { className: className, exact: true, to: '/', activeClassName: activeClassName },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'svg',
          { xmlns: 'http://www.w3.org/2000/svg', width: '52.103', height: '50.288', viewBox: '0 0 52.103 50.288' },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('path', { fill: '#fff', d: 'M0 0h14.77v14.77H0zM18.667 0h14.77v14.77h-14.77zM37.333 0h14.77v14.77h-14.77zM0 17.917h14.77v14.77H0zM18.667 17.917h14.77v14.77h-14.77zM37.333 17.917h14.77v14.77h-14.77zM0 35.52h14.77v14.768H0zM18.667 35.52h14.77v14.768h-14.77zM37.333 35.52h14.77v14.768h-14.77z' })
        ),
        p.t('list_link_text')
      ),
      __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        __WEBPACK_IMPORTED_MODULE_2_react_router_dom__["NavLink"],
        { className: className, to: '/map', activeClassName: activeClassName },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'svg',
          { xmlns: 'http://www.w3.org/2000/svg', width: '20.469', height: '47.778', viewBox: '0 0 20.469 47.778' },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('circle', { fill: '#E64D38', cx: '10.235', cy: '10.234', r: '10.234' }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('path', { fill: '#E64D38', d: 'M13.37 44.21c.007-1.327-.713-2.486-1.784-3.11l.12-30.94-3.57-.014-.12 30.94c-1.075.615-1.804 1.768-1.81 3.096-.006 1.978 1.59 3.588 3.57 3.595 1.977.008 3.587-1.59 3.595-3.567z' })
        ),
        p.t('map_link_text')
      )
    )
  );
};

Menu.propTypes = {
  // visible: PropTypes.bool.isRequired,
};

/* harmony default export */ __webpack_exports__["a"] = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_redux__["compose"])(__WEBPACK_IMPORTED_MODULE_2_react_router_dom__["withRouter"], __WEBPACK_IMPORTED_MODULE_4_redux_polyglot_translate___default.a)(Menu);

/***/ }),
/* 37 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);


var NotFound = function NotFound() {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
    'div',
    null,
    '404 Not Found'
  );
};

/* harmony default export */ __webpack_exports__["a"] = NotFound;

/***/ }),
/* 38 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }



var VideoItem = function (_React$Component) {
  _inherits(VideoItem, _React$Component);

  function VideoItem(props) {
    _classCallCheck(this, VideoItem);

    var _this = _possibleConstructorReturn(this, (VideoItem.__proto__ || Object.getPrototypeOf(VideoItem)).call(this, props));

    _this.state = {
      clicked: false
    };
    _this.handleClick = _this.handleClick.bind(_this);
    return _this;
  }

  _createClass(VideoItem, [{
    key: "handleClick",
    value: function handleClick() {
      this.setState({ clicked: true });
    }
  }, {
    key: "render",
    value: function render() {
      var video = this.props.video;
      var clicked = this.state.clicked;


      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        "div",
        { className: "image-wrapper" },
        clicked ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("iframe", { src: "https://www.youtube.com/embed/qREKP9oijWI?autoplay=1", frameBorder: "0", allowFullScreen: true }) : __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          "div",
          null,
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("img", { src: video.thumbnail_url, alt: "" }),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            "button",
            { onClick: this.handleClick, className: "play-button" },
            "Play"
          )
        )
      );
    }
  }]);

  return VideoItem;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = VideoItem;

/***/ }),
/* 39 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_facebook__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_facebook___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_facebook__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_classnames__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_classnames___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_classnames__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react_helmet__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react_helmet___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_react_helmet__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__WhatsBigItem__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__actions_item__ = __webpack_require__(13);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* global __CONFIG__ */








var Single = function (_Component) {
  _inherits(Single, _Component);

  function Single() {
    _classCallCheck(this, Single);

    return _possibleConstructorReturn(this, (Single.__proto__ || Object.getPrototypeOf(Single)).apply(this, arguments));
  }

  _createClass(Single, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.props.onGetItem(this.props.match.params.id);
    }
  }, {
    key: 'render',
    value: function render() {
      var item = this.props.item;

      var title = item.data ? item.data.title : '';

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        {
          className: __WEBPACK_IMPORTED_MODULE_3_classnames___default()({
            single: true,
            loading: true
          })
        },
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_4_react_helmet__["Helmet"],
          null,
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'title',
            null,
            'Whats Big - ' + title
          )
        ),
        item.data ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__WhatsBigItem__["a" /* default */], { item: item.data }) : null,
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'comments' },
           false ? React.createElement(
            FacebookProvider,
            { appID: __CONFIG__.FACEBOOK_APP_ID },
            React.createElement(Comments, { href: window.location.href })
          ) : null
        )
      );
    }
  }]);

  return Single;
}(__WEBPACK_IMPORTED_MODULE_0_react__["Component"]);

Single.propTypes = {
  summary: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].object,
  gridFields: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].array,
  actions: __WEBPACK_IMPORTED_MODULE_0_react__["PropTypes"].object
};


var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onGetItem: function onGetItem(alias) {
      dispatch(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_6__actions_item__["a" /* loadItem */])(alias));
    }
  };
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    item: state.item,
    loading: state.item.isLoading,
    guid: state.item.guid
  };
};

/* harmony default export */ __webpack_exports__["a"] = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_react_redux__["connect"])(mapStateToProps, mapDispatchToProps)(Single);

/***/ }),
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_helmet__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_react_helmet___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_react_helmet__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_polyglot_translate__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_redux_polyglot_translate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_redux_polyglot_translate__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_DropDown__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_List__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__actions_list__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__actions_settings__ = __webpack_require__(6);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }











var WhatsBigList = function (_React$Component) {
  _inherits(WhatsBigList, _React$Component);

  function WhatsBigList(props) {
    _classCallCheck(this, WhatsBigList);

    var _this = _possibleConstructorReturn(this, (WhatsBigList.__proto__ || Object.getPrototypeOf(WhatsBigList)).call(this, props));

    _this.handleCountryChanged = _this.handleCountryChanged.bind(_this);
    return _this;
  }

  _createClass(WhatsBigList, [{
    key: 'componentDidMount',
    value: function componentDidMount() {}
  }, {
    key: 'handleCountryChanged',
    value: function handleCountryChanged(value) {
      this.props.onChangeCountry(value);
      this.props.onGetList(value, this.props.settings.languageCode);
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          settings = _props.settings,
          items = _props.items,
          more = _props.more,
          loading = _props.loading,
          loadingMore = _props.loadingMore,
          onLoadMore = _props.onLoadMore,
          globalisation = _props.globalisation;

      var countryOptions = globalisation.countries.map(function (country) {
        return {
          value: country.country_code,
          label: country.name
        };
      });

      var selectedCountry = globalisation.countries.filter(function (country) {
        return country.country_code === settings.countryCode;
      });

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        null,
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_3_react_helmet__["Helmet"],
          null,
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'title',
            null,
            'What\'s Big - List'
          )
        ),
        loading ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'loader' }) : __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          'div',
          { className: 'list-page-wrapper' },
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'div',
            { className: 'list-page-header' },
            globalisation.countries.length ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
              'div',
              { className: 'country-dropdown-wrapper' },
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
                'span',
                null,
                this.props.p.t('currently_trending_text')
              ),
              __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_5__components_DropDown__["a" /* default */], {
                className: 'dropdown--country',
                value: settings.countryCode,
                label: selectedCountry.length ? selectedCountry[0].title_name : '',
                options: countryOptions,
                onChange: this.handleCountryChanged
              })
            ) : null
          ),
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_6__components_List__["a" /* default */], { more: more, loadingMore: loadingMore, onLoadMore: onLoadMore, items: items })
        )
      );
    }
  }]);

  return WhatsBigList;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onLoadMore: function onLoadMore(countryCode, languageCode, page) {
      dispatch(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_7__actions_list__["b" /* loadListMore */])(countryCode, languageCode, page));
    },
    onGetList: function onGetList(countryCode, languageCode) {
      dispatch(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_7__actions_list__["a" /* loadList */])(countryCode, languageCode));
    },
    onChangeCountry: function onChangeCountry(countryCode) {
      dispatch(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_8__actions_settings__["c" /* setCountry */])(countryCode));
    }
  };
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    globalisation: state.globalisation,
    items: state.list.items,
    loading: state.list.isLoading,
    loadingMore: state.list.isLoadingMore,
    more: state.list.more,
    guid: state.list.guid,
    settings: state.settings
  };
};

/* harmony default export */ __webpack_exports__["a"] = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_redux__["compose"])(__WEBPACK_IMPORTED_MODULE_4_redux_polyglot_translate___default.a, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_react_redux__["connect"])(mapStateToProps, mapDispatchToProps))(WhatsBigList);

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_helmet__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_react_helmet___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_react_helmet__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react_router_dom__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react_router_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_react_router_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_redux_polyglot_translate__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_redux_polyglot_translate___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_redux_polyglot_translate__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_classnames__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_classnames___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_classnames__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_MapView__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__actions_list__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__actions_settings__ = __webpack_require__(6);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/* global __CONFIG__ */











var WhatsBigMap = function (_React$Component) {
  _inherits(WhatsBigMap, _React$Component);

  function WhatsBigMap(props) {
    _classCallCheck(this, WhatsBigMap);

    var _this = _possibleConstructorReturn(this, (WhatsBigMap.__proto__ || Object.getPrototypeOf(WhatsBigMap)).call(this, props));

    _this.state = {
      loading: true,
      apiReady: false
    };
    _this.mapsApiReady = _this.mapsApiReady.bind(_this);
    return _this;
  }

  _createClass(WhatsBigMap, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.initMapsApi();
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var _this2 = this;

      if (this.props.globalisation.countries.length) {
        var countryFromUrl = this.props.globalisation.countries.filter(function (c) {
          return c.name.toLowerCase() === (nextProps.match.params.country || '').toLowerCase();
        })[0];
        var countryFromSettings = this.props.globalisation.countries.filter(function (c) {
          return c.country_code === _this2.props.settings.countryCode;
        })[0];

        var country = countryFromUrl || countryFromSettings;
        if (country.country_code !== this.props.settings.countryCode) {
          this.props.setCountry(country.country_code);
          this.props.onGetList(country.country_code, this.props.settings.languageCode);
        }
      }
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      return true;
    }
  }, {
    key: 'initMapsApi',
    value: function initMapsApi() {
      if (window.google && window.google.maps) {
        this.mapsApiReady();
        return;
      }
      var global = 'nd_const' + Math.round(Math.random() * 99999);
      window[global] = this.mapsApiReady;
      var script = document.createElement('script');
      script.onload = function () {
        return delete window[global];
      };
      script.type = 'text/javascript';
      script.src = 'https://maps.googleapis.com/maps/api/js?callback=' + global + '&key=' + __webpack_require__.i({"ENVIRONMENT":"development","IS_CLIENT":false,"IS_SERVER":true,"DEVELOPMENT":true,"ENABLE_DEVTOOLS":false,"API_URL":undefined,"BASE_URL":undefined,"COOKIE_NAME":undefined}).GOOGLE_MAPS_API_KEY;
      document.body.appendChild(script);
    }
  }, {
    key: 'mapsApiReady',
    value: function mapsApiReady() {
      this.setState({ apiReady: true });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          globalisation = _props.globalisation,
          settings = _props.settings;

      var selectedCountry = globalisation.countries.filter(function (country) {
        return country.country_code === settings.countryCode;
      });
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
        'div',
        null,
        __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
          __WEBPACK_IMPORTED_MODULE_2_react_helmet__["Helmet"],
          null,
          __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
            'title',
            null,
            'What\'s Big - Map'
          )
        ),
        this.state.apiReady && selectedCountry.length && globalisation.countries.length ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_7__components_MapView__["a" /* default */], { title: this.props.p.t('currently_trending_text') + ' ' + selectedCountry[0].title_name, selectedCountry: selectedCountry[0], countries: globalisation.countries, items: this.props.items }) : __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement('div', { className: 'loader' })
      );
    }
  }]);

  return WhatsBigMap;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    onGetList: function onGetList(countryCode, languageCode) {
      dispatch(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_8__actions_list__["a" /* loadList */])(countryCode, languageCode));
    },
    setCountry: function setCountry(countryCode) {
      dispatch(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_9__actions_settings__["c" /* setCountry */])(countryCode));
    }
  };
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    items: state.list.items,
    loading: state.list.isLoading,
    more: state.list.more,
    guid: state.list.guid,
    settings: state.settings,
    globalisation: state.globalisation
  };
};

/* harmony default export */ __webpack_exports__["a"] = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3_redux__["compose"])(__WEBPACK_IMPORTED_MODULE_5_redux_polyglot_translate___default.a, __WEBPACK_IMPORTED_MODULE_4_react_router_dom__["withRouter"], __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1_react_redux__["connect"])(mapStateToProps, mapDispatchToProps))(WhatsBigMap);

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux_pack__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux_pack___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_redux_pack__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__actions_feedback__ = __webpack_require__(11);
/* harmony export (immutable) */ __webpack_exports__["a"] = reducer;
var _actionsMap;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var initialState = {
  isSending: false,
  error: false,
  sent: false
};

var actionsMap = (_actionsMap = {}, _defineProperty(_actionsMap, __WEBPACK_IMPORTED_MODULE_1__actions_feedback__["c" /* SEND_FEEDBACK */], function (state, action) {
  return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_redux_pack__["handle"])(state, action, {
    start: function start(s) {
      return _extends({}, s, { isSending: true, error: false, sent: false });
    },
    finish: function finish(s) {
      return _extends({}, s, { isSending: false });
    },
    failure: function failure(s) {
      return _extends({}, s, { error: true });
    },
    success: function success(s) {
      return _extends({}, s, { error: false, sent: true });
    },
    always: function always(s) {
      return s;
    }
  });
}), _defineProperty(_actionsMap, __WEBPACK_IMPORTED_MODULE_1__actions_feedback__["d" /* RESET_FEEDBACK */], function () {
  return initialState;
}), _actionsMap);

function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}

/***/ }),
/* 43 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux_pack__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux_pack___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_redux_pack__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__actions_globalisation__ = __webpack_require__(12);
/* harmony export (immutable) */ __webpack_exports__["a"] = reducer;
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var initialState = {
  isLoading: false,
  error: null,
  countries: [],
  languages: []
};

var actionsMap = _defineProperty({}, __WEBPACK_IMPORTED_MODULE_1__actions_globalisation__["b" /* LOAD_GLOBALISATION_DATA */], function (state, action) {
  return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_redux_pack__["handle"])(state, action, {
    start: function start(s) {
      return _extends({}, s, { isLoading: true, error: null });
    },
    finish: function finish(s) {
      return _extends({}, s, { isLoading: false });
    },
    failure: function failure(s) {
      return _extends({}, s, { error: action.payload.error });
    },
    success: function success(s) {
      return _extends({}, s, {
        countries: action.payload.countries,
        languages: action.payload.languages,
        more: action.payload.more
      });
    },
    always: function always(s) {
      return s;
    }
  });
});

function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}

/***/ }),
/* 44 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux_polyglot__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_redux_polyglot___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_redux_polyglot__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux_form__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_redux_form___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_redux_form__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__settings__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__list__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__feedback__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__item__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__globalisation__ = __webpack_require__(43);










/* harmony default export */ __webpack_exports__["a"] = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_redux__["combineReducers"])({
  globalisation: __WEBPACK_IMPORTED_MODULE_7__globalisation__["a" /* default */],
  feedback: __WEBPACK_IMPORTED_MODULE_5__feedback__["a" /* default */],
  settings: __WEBPACK_IMPORTED_MODULE_3__settings__["a" /* default */],
  list: __WEBPACK_IMPORTED_MODULE_4__list__["a" /* default */],
  item: __WEBPACK_IMPORTED_MODULE_6__item__["a" /* default */],
  polyglot: __WEBPACK_IMPORTED_MODULE_1_redux_polyglot__["polyglotReducer"],
  form: __WEBPACK_IMPORTED_MODULE_2_redux_form__["reducer"]
});

/***/ }),
/* 45 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux_pack__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux_pack___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_redux_pack__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__actions_item__ = __webpack_require__(13);
/* harmony export (immutable) */ __webpack_exports__["a"] = reducer;
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





var initialState = {
  isLoading: false,
  error: null,
  data: null
};

var actionsMap = _defineProperty({}, __WEBPACK_IMPORTED_MODULE_1__actions_item__["b" /* LOAD_ITEM */], function (state, action) {
  return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_redux_pack__["handle"])(state, action, {
    start: function start(s) {
      return _extends({}, s, { isLoading: true, error: null, data: null });
    },
    finish: function finish(s) {
      return _extends({}, s, { isLoading: false });
    },
    failure: function failure(s) {
      return _extends({}, s, { error: action.payload.error });
    },
    success: function success(s) {
      return _extends({}, s, { data: action.payload.data });
    },
    always: function always(s) {
      return s;
    }
  });
});

function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}

/***/ }),
/* 46 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux_pack__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_redux_pack___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_redux_pack__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__actions_list__ = __webpack_require__(8);
/* harmony export (immutable) */ __webpack_exports__["a"] = reducer;
var _actionsMap;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }





var initialState = {
  isLoading: false,
  isLoadingMore: false,
  error: null,
  guid: null,
  items: [],
  more: false,
  page: 0
};

var actionsMap = (_actionsMap = {}, _defineProperty(_actionsMap, __WEBPACK_IMPORTED_MODULE_1__actions_list__["c" /* LOAD_LIST */], function (state, action) {
  return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_redux_pack__["handle"])(state, action, {
    start: function start(s) {
      return _extends({}, s, { isLoading: true, error: null, items: [] });
    },
    finish: function finish(s) {
      return _extends({}, s, { isLoading: false });
    },
    failure: function failure(s) {
      return _extends({}, s, { error: action.payload.error });
    },
    success: function success(s) {
      return _extends({}, s, { items: action.payload.items, more: action.payload.more, guid: action.payload.guid });
    },
    always: function always(s) {
      return s;
    }
  });
}), _defineProperty(_actionsMap, __WEBPACK_IMPORTED_MODULE_1__actions_list__["d" /* LOAD_LIST_MORE */], function (state, action) {
  return __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0_redux_pack__["handle"])(state, action, {
    start: function start(s) {
      return _extends({}, s, { isLoadingMore: true, error: null });
    },
    finish: function finish(s) {
      return _extends({}, s, { isLoadingMore: false });
    },
    failure: function failure(s) {
      return _extends({}, s, { error: action.payload.error });
    },
    success: function success(s) {
      return _extends({}, s, { items: [].concat(_toConsumableArray(state.items), _toConsumableArray(action.payload.items)), more: action.payload.more });
    },
    always: function always(s) {
      return s;
    }
  });
}), _actionsMap);

function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}

/***/ }),
/* 47 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__actions_settings__ = __webpack_require__(6);
/* harmony export (immutable) */ __webpack_exports__["a"] = reducer;
var _actionsMap;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var initialState = {
  languageCode: 'en',
  countryCode: 'GB',
  countryIndex: 0,
  menuVisible: false
};

var actionsMap = (_actionsMap = {}, _defineProperty(_actionsMap, __WEBPACK_IMPORTED_MODULE_0__actions_settings__["d" /* SET_MENU_VISIBLE */], function (state, action) {
  return _extends({}, state, { menuVisible: action.visible });
}), _defineProperty(_actionsMap, __WEBPACK_IMPORTED_MODULE_0__actions_settings__["e" /* SET_COUNTRY */], function (state, action) {
  return _extends({}, state, { countryCode: action.countryCode });
}), _defineProperty(_actionsMap, __WEBPACK_IMPORTED_MODULE_0__actions_settings__["f" /* SET_LANGUAGE */], function (state, action) {
  return _extends({}, state, { languageCode: action.languageCode });
}), _actionsMap);

function reducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}

/***/ }),
/* 48 */
/***/ (function(module, exports) {

module.exports = {
	"geo_location_data": {
		"country_code": "GB",
		"language_code": "en"
	},
	"guid": "c8720fc5-a3e8-4e58-86aa-7a959e3a7868",
	"items": [
		{
			"title": "Some content term",
			"alias": "dHJkh786",
			"content_pages": [
				{
					"url": "http://news.distractify.com/culture/a-runner-fell-in-a-race-but-then-she-got-back-up-and-taught-everyone-a-major-life-lesson/",
					"title": "This Runner Had A Painful Fall. Then She Stunned The Entire Crowd By Doing This!",
					"description": "",
					"thumbnail_url": "http://www.runninglau.com/wp-content/uploads/2014/02/woman-stretching-runner-female-asian-landscape-sunset-yellow-1.jpg"
				}
			],
			"images": [],
			"youtube_videos": [],
			"share_caption": "\"Some content term\" is currently big on http://www.whatsbig.com",
			"share_thumbnail": "http://www.runninglau.com/wp-content/uploads/2014/02/woman-stretching-runner-female-asian-landscape-sunset-yellow-1.jpg"
		},
		{
			"title": "#SomeHashTag",
			"alias": "sfkh76HG",
			"content_pages": [
				{
					"url": "http://news.distractify.com/culture/a-runner-fell-in-a-race-but-then-she-got-back-up-and-taught-everyone-a-major-life-lesson/",
					"title": "This Runner Had A Painful Fall. Then She Stunned The Entire Crowd By Doing This!",
					"description": "",
					"thumbnail_url": "http://www.runninglau.com/wp-content/uploads/2014/02/woman-stretching-runner-female-asian-landscape-sunset-yellow-1.jpg"
				},
				{
					"url": "http://www.bbc.co.uk/news/uk-politics-27587439",
					"title": "'Post-Clegg' opinion poll 'inexcusable', says Cable",
					"description": "Vince Cable says a opinion poll commissioned by a Lib Dem peer into Nick Clegg&#039;s electoral appeal was &quot;totally inexcusable&quot;.",
					"thumbnail_url": "http://news.bbcimg.co.uk/media/images/75131000/jpg/_75131279_75131278.jpg"
				},
				{
					"url": "http://news.distractify.com/people/will-ferrell-and-chad-smith-had-an-epic-drum-off-and-it-will-melt-your-mind-in-more-than-one-way/",
					"title": "Will Ferrell And Chad Smith Had An Epic Drum-Off, And It Will Melt Your Mind In More Than One Way",
					"description": "",
					"thumbnail_url": ""
				}
			],
			"images": [],
			"youtube_videos": [],
			"share_caption": "\"#SomeHashTag\" is currently big on http://www.whatsbig.com",
			"share_thumbnail": "http://www.runninglau.com/wp-content/uploads/2014/02/woman-stretching-runner-female-asian-landscape-sunset-yellow-1.jpg"
		},
		{
			"title": "#SomethingHappened",
			"alias": "kjh345Jj",
			"content_pages": [
				{
					"url": "http://news.distractify.com/people/will-ferrell-and-chad-smith-had-an-epic-drum-off-and-it-will-melt-your-mind-in-more-than-one-way/",
					"title": "Will Ferrell And Chad Smith Had An Epic Drum-Off, And It Will Melt Your Mind In More Than One Way",
					"description": "",
					"thumbnail_url": ""
				},
				{
					"url": "http://www.bbc.co.uk/news/uk-politics-27587439",
					"title": "'Post-Clegg' opinion poll 'inexcusable', says Cable",
					"description": "Vince Cable says a opinion poll commissioned by a Lib Dem peer into Nick Clegg&#039;s electoral appeal was &quot;totally inexcusable&quot;.",
					"thumbnail_url": "http://news.bbcimg.co.uk/media/images/75131000/jpg/_75131279_75131278.jpg"
				},
				{
					"url": "http://news.distractify.com/culture/a-runner-fell-in-a-race-but-then-she-got-back-up-and-taught-everyone-a-major-life-lesson/",
					"title": "This Runner Had A Painful Fall. Then She Stunned The Entire Crowd By Doing This!",
					"description": "",
					"thumbnail_url": "http://www.runninglau.com/wp-content/uploads/2014/02/woman-stretching-runner-female-asian-landscape-sunset-yellow-1.jpg"
				}
			],
			"images": [],
			"youtube_videos": [
				{
					"url": "https://www.youtube.com/watch?v=vTQZBdnDYzs",
					"title": "EPIC CHIPOTLE PRANK GONE WRONG!",
					"short_description": "Thank you for watching! If you enjoyed, please Subscribe by clicking here http://bit.ly/1jJ9uw1 I try to do videos every week! :D Do we have enough love and ...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/vTQZBdnDYzs/maxresdefault.jpg"
				}
			],
			"share_caption": "\"#SomethingHappened\" is currently big on http://www.whatsbig.com",
			"share_thumbnail": "https://i1.ytimg.com/vi/vTQZBdnDYzs/maxresdefault.jpg"
		},
		{
			"title": "#AnotherHashtag",
			"alias": "MNPO87t6",
			"content_pages": [
				{
					"url": "http://www.bbc.co.uk/news/uk-politics-27587439",
					"title": "'Post-Clegg' opinion poll 'inexcusable', says Cable",
					"description": "Vince Cable says a opinion poll commissioned by a Lib Dem peer into Nick Clegg&#039;s electoral appeal was &quot;totally inexcusable&quot;.",
					"thumbnail_url": "http://news.bbcimg.co.uk/media/images/75131000/jpg/_75131279_75131278.jpg"
				},
				{
					"url": "http://news.distractify.com/people/will-ferrell-and-chad-smith-had-an-epic-drum-off-and-it-will-melt-your-mind-in-more-than-one-way/",
					"title": "Will Ferrell And Chad Smith Had An Epic Drum-Off, And It Will Melt Your Mind In More Than One Way",
					"description": "",
					"thumbnail_url": ""
				},
				{
					"url": "http://news.distractify.com/culture/a-runner-fell-in-a-race-but-then-she-got-back-up-and-taught-everyone-a-major-life-lesson/",
					"title": "This Runner Had A Painful Fall. Then She Stunned The Entire Crowd By Doing This!",
					"description": "",
					"thumbnail_url": "http://www.runninglau.com/wp-content/uploads/2014/02/woman-stretching-runner-female-asian-landscape-sunset-yellow-1.jpg"
				}
			],
			"images": [],
			"youtube_videos": [
				{
					"url": "https://www.youtube.com/watch?v=-uTk1_Cruq4",
					"title": "YYYYYYEEEESSSSSS!!!!! - FIFA 14 PACK OPENING",
					"short_description": "Follow Me On Twitter: https://twitter.com/ComedyGamer ►Cheap + INSTANT COINS - http://www.thegamekeys.co.uk Discount code &#39;Deji&#39; for 5% off! Music used: Exci...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/-uTk1_Cruq4/maxresdefault.jpg"
				},
				{
					"url": "https://www.youtube.com/watch?v=vTQZBdnDYzs",
					"title": "EPIC CHIPOTLE PRANK GONE WRONG!",
					"short_description": "Thank you for watching! If you enjoyed, please Subscribe by clicking here http://bit.ly/1jJ9uw1 I try to do videos every week! :D Do we have enough love and ...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/vTQZBdnDYzs/maxresdefault.jpg"
				},
				{
					"url": "https://www.youtube.com/watch?v=r3b_Ype5cKE",
					"title": "Toju is Ant and Dec's Golden Buzzer act | Britain's Got Talent 2014",
					"short_description": "See more from Britain&#39;s Got Talent at http://itv.com/talent Toju has the Judges roaring with laughter with his stand-up routine. When Ant and Dec burst out f...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/r3b_Ype5cKE/hqdefault.jpg"
				}
			],
			"share_caption": "\"#AnotherHashtag\" is currently big on http://www.whatsbig.com",
			"share_thumbnail": "https://i1.ytimg.com/vi/-uTk1_Cruq4/maxresdefault.jpg"
		},
		{
			"title": "#WTF",
			"alias": "fdjHYG89",
			"content_pages": [],
			"images": [
				{
					"url": "https://pbs.twimg.com/media/BoqsHBFIAAAz8vo.jpg:large",
					"caption": "GET READY. This episode is going to make history. #10MinutesToGo #WhoKillsTina #Corrie"
				}
			],
			"youtube_videos": [],
			"share_caption": "\"#WTF\" is currently big on http://www.whatsbig.com",
			"share_thumbnail": "https://pbs.twimg.com/media/BoqsHBFIAAAz8vo.jpg:large"
		},
		{
			"title": "Image term",
			"alias": "pq87OIop",
			"content_pages": [],
			"images": [
				{
					"url": "https://pbs.twimg.com/media/Boq3zZ4IgAEI5vx.jpg:large",
					"caption": "Nice touch when you try to scan yourself through a security camera. #Watch_Dogs #PS4share"
				},
				{
					"url": "https://pbs.twimg.com/media/Boq3eMaIgAE-W_f.jpg:large",
					"caption": "Ayyyy #WatchDogs bb"
				},
				{
					"url": "https://pbs.twimg.com/media/BoqumnlCcAASIL3.jpg:large",
					"caption": "Your votes have been counted and the person you think kills Tina is Rob! Here we go people! #WhoKillsTina #Corrie"
				},
				{
					"url": "https://pbs.twimg.com/media/BoqsHBFIAAAz8vo.jpg:large",
					"caption": "GET READY. This episode is going to make history. #10MinutesToGo #WhoKillsTina #Corrie"
				},
				{
					"url": "https://pbs.twimg.com/media/BopH3q3CIAAW2Rv.png:large",
					"caption": "LOL! There's a herd of these at #SIF14 RT @NoisySq @ioerror @ggreenwald #neverasktherealquestionsconference"
				},
				{
					"url": "https://pbs.twimg.com/media/BopawuqCEAElOHS.jpg:large",
					"caption": "Thanks @nnenna for taking the photo of us at  panel on the @UNESCO #InternetStudy here in Stockholm. #SIF14"
				},
				{
					"url": "https://pbs.twimg.com/media/Boo9rXBIUAAOpbA.png:large",
					"caption": "As #SIF14 deals w/web freedom, anniversary of Gezi \"Lady in Red\" photo. Reminder of all that happned since. #Turkey"
				},
				{
					"url": "https://pbs.twimg.com/media/BolCeXXIgAEuax2.jpg:large",
					"caption": "@nnenna of @webfoundation recognises our missing Ethiopian colleagues at #SIF14 opening  #FreeZone9Bloggers"
				},
				{
					"url": "https://pbs.twimg.com/media/Bok_RRNIIAA9nG-.jpg:large",
					"caption": "And here's one of them #PT Minister for International Development Cooperation, Hillevi Engström #SIF14"
				},
				{
					"url": "https://pbs.twimg.com/media/BoVPTUcIcAE7_8T.jpg:large",
					"caption": "Ppl say 'nothing 2 hide' but 86% of US#internet users apparently taken steps 2 remove/mask digital footprints"
				}
			],
			"youtube_videos": [],
			"share_caption": "\"Image term\" is currently big on http://www.whatsbig.com",
			"share_thumbnail": "https://pbs.twimg.com/media/BoqsHBFIAAAz8vo.jpg:large"
		},
		{
			"title": "What happened here",
			"alias": "gt09skuj",
			"content_pages": [],
			"images": [],
			"youtube_videos": [
				{
					"url": "https://www.youtube.com/watch?v=-uTk1_Cruq4",
					"title": "YYYYYYEEEESSSSSS!!!!! - FIFA 14 PACK OPENING",
					"short_description": "Follow Me On Twitter: https://twitter.com/ComedyGamer ►Cheap + INSTANT COINS - http://www.thegamekeys.co.uk Discount code &#39;Deji&#39; for 5% off! Music used: Exci...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/-uTk1_Cruq4/maxresdefault.jpg"
				}
			],
			"share_caption": "\"What happened here\" is currently big on http://www.whatsbig.com",
			"share_thumbnail": "https://i1.ytimg.com/vi/-uTk1_Cruq4/maxresdefault.jpg"
		},
		{
			"title": "Videos term",
			"alias": "54608OIU",
			"content_pages": [],
			"images": [],
			"youtube_videos": [
				{
					"url": "https://www.youtube.com/watch?v=r3b_Ype5cKE",
					"title": "Toju is Ant and Dec's Golden Buzzer act | Britain's Got Talent 2014",
					"short_description": "See more from Britain&#39;s Got Talent at http://itv.com/talent Toju has the Judges roaring with laughter with his stand-up routine. When Ant and Dec burst out f...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/r3b_Ype5cKE/hqdefault.jpg"
				},
				{
					"url": "https://www.youtube.com/watch?v=-uTk1_Cruq4",
					"title": "YYYYYYEEEESSSSSS!!!!! - FIFA 14 PACK OPENING",
					"short_description": "Follow Me On Twitter: https://twitter.com/ComedyGamer ►Cheap + INSTANT COINS - http://www.thegamekeys.co.uk Discount code &#39;Deji&#39; for 5% off! Music used: Exci...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/-uTk1_Cruq4/maxresdefault.jpg"
				},
				{
					"url": "https://www.youtube.com/watch?v=vTQZBdnDYzs",
					"title": "EPIC CHIPOTLE PRANK GONE WRONG!",
					"short_description": "Thank you for watching! If you enjoyed, please Subscribe by clicking here http://bit.ly/1jJ9uw1 I try to do videos every week! :D Do we have enough love and ...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/vTQZBdnDYzs/maxresdefault.jpg"
				}
			],
			"share_caption": "\"Videos term\" is currently big on http://www.whatsbig.com",
			"share_thumbnail": "https://i1.ytimg.com/vi/r3b_Ype5cKE/hqdefault.jpg"
		},
		{
			"title": "Not many images",
			"alias": "plmnjoYg",
			"content_pages": [],
			"images": [
				{
					"url": "https://pbs.twimg.com/media/Boq3zZ4IgAEI5vx.jpg:large",
					"caption": "Nice touch when you try to scan yourself through a security camera. #Watch_Dogs #PS4share"
				},
				{
					"url": "https://pbs.twimg.com/media/Boq3eMaIgAE-W_f.jpg:large",
					"caption": "Ayyyy #WatchDogs bb"
				},
				{
					"url": "https://pbs.twimg.com/media/BoqumnlCcAASIL3.jpg:large",
					"caption": "Your votes have been counted and the person you think kills Tina is Rob! Here we go people! #WhoKillsTina #Corrie"
				}
			],
			"youtube_videos": [
				{
					"url": "https://www.youtube.com/watch?v=vTQZBdnDYzs",
					"title": "EPIC CHIPOTLE PRANK GONE WRONG!",
					"short_description": "Thank you for watching! If you enjoyed, please Subscribe by clicking here http://bit.ly/1jJ9uw1 I try to do videos every week! :D Do we have enough love and ...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/vTQZBdnDYzs/maxresdefault.jpg"
				},
				{
					"url": "https://www.youtube.com/watch?v=r3b_Ype5cKE",
					"title": "Toju is Ant and Dec's Golden Buzzer act | Britain's Got Talent 2014",
					"short_description": "See more from Britain&#39;s Got Talent at http://itv.com/talent Toju has the Judges roaring with laughter with his stand-up routine. When Ant and Dec burst out f...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/r3b_Ype5cKE/hqdefault.jpg"
				},
				{
					"url": "https://www.youtube.com/watch?v=-uTk1_Cruq4",
					"title": "YYYYYYEEEESSSSSS!!!!! - FIFA 14 PACK OPENING",
					"short_description": "Follow Me On Twitter: https://twitter.com/ComedyGamer ►Cheap + INSTANT COINS - http://www.thegamekeys.co.uk Discount code &#39;Deji&#39; for 5% off! Music used: Exci...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/-uTk1_Cruq4/maxresdefault.jpg"
				}
			],
			"share_caption": "\"Not many images\" is currently big on http://www.whatsbig.com",
			"share_thumbnail": "http://news.bbcimg.co.uk/media/images/75131000/jpg/_75131279_75131278.jpg"
		},
		{
			"title": "Content term",
			"alias": "plmnjoYg",
			"content_pages": [
				{
					"url": "http://www.bbc.co.uk/news/uk-politics-27587439",
					"title": "'Post-Clegg' opinion poll 'inexcusable', says Cable",
					"description": "Vince Cable says a opinion poll commissioned by a Lib Dem peer into Nick Clegg&#039;s electoral appeal was &quot;totally inexcusable&quot;.",
					"thumbnail_url": "http://news.bbcimg.co.uk/media/images/75131000/jpg/_75131279_75131278.jpg"
				},
				{
					"url": "http://news.distractify.com/people/will-ferrell-and-chad-smith-had-an-epic-drum-off-and-it-will-melt-your-mind-in-more-than-one-way/",
					"title": "Will Ferrell And Chad Smith Had An Epic Drum-Off, And It Will Melt Your Mind In More Than One Way",
					"description": "",
					"thumbnail_url": ""
				},
				{
					"url": "http://news.distractify.com/culture/a-runner-fell-in-a-race-but-then-she-got-back-up-and-taught-everyone-a-major-life-lesson/",
					"title": "This Runner Had A Painful Fall. Then She Stunned The Entire Crowd By Doing This!",
					"description": "",
					"thumbnail_url": "http://www.runninglau.com/wp-content/uploads/2014/02/woman-stretching-runner-female-asian-landscape-sunset-yellow-1.jpg"
				}
			],
			"images": [
				{
					"url": "https://pbs.twimg.com/media/Boq3eMaIgAE-W_f.jpg:large",
					"caption": "Ayyyy #WatchDogs bb"
				},
				{
					"url": "https://pbs.twimg.com/media/Boq3zZ4IgAEI5vx.jpg:large",
					"caption": "Nice touch when you try to scan yourself through a security camera. #Watch_Dogs #PS4share"
				}
			],
			"youtube_videos": [
				{
					"url": "https://www.youtube.com/watch?v=vTQZBdnDYzs",
					"title": "EPIC CHIPOTLE PRANK GONE WRONG!",
					"short_description": "Thank you for watching! If you enjoyed, please Subscribe by clicking here http://bit.ly/1jJ9uw1 I try to do videos every week! :D Do we have enough love and ...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/vTQZBdnDYzs/maxresdefault.jpg"
				},
				{
					"url": "https://www.youtube.com/watch?v=-uTk1_Cruq4",
					"title": "YYYYYYEEEESSSSSS!!!!! - FIFA 14 PACK OPENING",
					"short_description": "Follow Me On Twitter: https://twitter.com/ComedyGamer ►Cheap + INSTANT COINS - http://www.thegamekeys.co.uk Discount code &#39;Deji&#39; for 5% off! Music used: Exci...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/-uTk1_Cruq4/maxresdefault.jpg"
				},
				{
					"url": "https://www.youtube.com/watch?v=r3b_Ype5cKE",
					"title": "Toju is Ant and Dec's Golden Buzzer act | Britain's Got Talent 2014",
					"short_description": "See more from Britain&#39;s Got Talent at http://itv.com/talent Toju has the Judges roaring with laughter with his stand-up routine. When Ant and Dec burst out f...",
					"long_description": "",
					"thumbnail_url": "https://i1.ytimg.com/vi/r3b_Ype5cKE/hqdefault.jpg"
				}
			],
			"share_caption": "\"Content term\" is currently big on http://www.whatsbig.com",
			"share_thumbnail": "http://news.bbcimg.co.uk/media/images/75131000/jpg/_75131279_75131278.jpg"
		},
		{
			"title": "Single content item",
			"alias": "plmnjoYg",
			"content_pages": [
				{
					"url": "http://news.distractify.com/people/will-ferrell-and-chad-smith-had-an-epic-drum-off-and-it-will-melt-your-mind-in-more-than-one-way/",
					"title": "Will Ferrell And Chad Smith Had An Epic Drum-Off, And It Will Melt Your Mind In More Than One Way",
					"description": "",
					"thumbnail_url": ""
				}
			],
			"images": [],
			"youtube_videos": [],
			"share_caption": "\"Single content item\" is currently big on http://www.whatsbig.com",
			"share_thumbnail": ""
		}
	],
	"more": true
};

/***/ }),
/* 49 */
/***/ (function(module, exports) {

module.exports = {
	"en": {
		"tagline": "What's trending on the internet around the world right now!",
		"list_link_text": "The What's Big? List",
		"map_link_text": "The What's Big? Map",
		"language_dropdown_title": "Language",
		"form_name_label": "Name",
		"form_phone_label": "Phone",
		"form_email_label": "Email",
		"form_comment_label": "Comment",
		"form_captcha_label": "Captcha",
		"form_ok_button_text": "Submit",
		"form_cancel_button_text": "Cancel",
		"form_required": "Required",
		"send_feedback_title": "Send us you feedback",
		"send_feedback_text": "Do you like our site? Is there anything that you want to see on this site? Tell us your thoughts and we'll try and make What's Big? even better for you.",
		"submission_error": "There was an error submitting your form. Please check the errors below.",
		"email_error": "Please enter a valid email",
		"comment_error": "Please enter a valid comment",
		"feedback_submission_success": "Thank you for your feedback, it is very much appreciated.",
		"feedback_submission_fail": "Oh dear. There was a problem receiving your comment. Please would you try again later?",
		"whats_trending_text": "What's trending around the world?",
		"currently_trending_text": "Currently trending in"
	},
	"sv": {
		"tagline": "Ta reda på vad som händer på internet just nu!",
		"list_link_text": "What's Big-listan",
		"map_link_text": "What's Big-kartan",
		"language_dropdown_title": "Språk",
		"form_name_label": "Namn",
		"form_phone_label": "Telefon",
		"form_email_label": "Email",
		"form_comment_label": "Kommentar",
		"form_captcha_label": "Captcha",
		"form_ok_button_text": "Skicka",
		"form_cancel_button_text": "Avbryt",
		"form_required": "Obligatoriskt",
		"send_feedback_title": "Skicka oss feedback",
		"send_feedback_text": "Gillar du vår sida? Finns det något vi skulle kunna göra bättre? Hör av dig med dina tankar så ska vi försöka göra What's Big? ännu bättre.",
		"submission_error": "Ett fel uppstod. Vänligen rätta till felen nedan.",
		"email_error": "Vänligen ange en korrekt email adress.",
		"comment_error": "Vänligen ange en kommentar.",
		"feedback_submission_success": "Tack för din feedback.",
		"feedback_submission_fail": "Oops. Ett fel uppstod. Vänligen försök igen senare.",
		"whats_trending_text": "Vad händer runt om världen?",
		"currently_trending_text": "Just nu i"
	}
};

/***/ }),
/* 50 */
/***/ (function(module, exports) {

module.exports = require("isomorphic-fetch");

/***/ }),
/* 51 */
/***/ (function(module, exports) {

module.exports = require("react-google-recaptcha");

/***/ }),
/* 52 */
/***/ (function(module, exports) {

module.exports = require("react-modal");

/***/ }),
/* 53 */
/***/ (function(module, exports) {

module.exports = require("redux-localstorage");

/***/ }),
/* 54 */
/***/ (function(module, exports) {

module.exports = require("redux-thunk");

/***/ }),
/* 55 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_serve_favicon__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_serve_favicon___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_serve_favicon__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_compression__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_compression___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_compression__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_path__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_path___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_path__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_dom_server__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react_dom_server___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react_dom_server__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_react_router_dom__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_react_router_dom___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_react_router_dom__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_react_redux__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_react_redux___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_react_redux__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__store__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__containers_App__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__index_html__ = __webpack_require__(23);












var PORT = process.env.PORT || 3000;

var app = new __WEBPACK_IMPORTED_MODULE_0_express___default.a();
app.use(__WEBPACK_IMPORTED_MODULE_2_compression___default()());
app.use(__WEBPACK_IMPORTED_MODULE_1_serve_favicon___default()(__WEBPACK_IMPORTED_MODULE_3_path___default.a.join(__dirname, '..', 'public', 'favicon.ico')));
app.use(__WEBPACK_IMPORTED_MODULE_0_express___default.a.static(__WEBPACK_IMPORTED_MODULE_3_path___default.a.join(__dirname, '..', 'public')));

app.get('*', function (req, res) {
  // This context object contains the results of the render
  var context = {};

  // Create a new Redux store instance
  var store = __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_8__store__["a" /* default */])();

  var html = __WEBPACK_IMPORTED_MODULE_5_react_dom_server___default.a.renderToString(__WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(
    __WEBPACK_IMPORTED_MODULE_7_react_redux__["Provider"],
    { store: store },
    __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(
      __WEBPACK_IMPORTED_MODULE_6_react_router_dom__["StaticRouter"],
      { context: {}, location: req.url },
      __WEBPACK_IMPORTED_MODULE_4_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_9__containers_App__["a" /* default */], null)
    )
  ));

  // Grab the initial state from our Redux store
  var preloadedState = store.getState();

  // context.url will contain the URL to redirect to if a <Redirect> was used
  if (context.url) {
    res.writeHead(302, {
      Location: context.url
    });
    res.end();
  } else {
    res.write(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_10__index_html__["a" /* default */])(html, preloadedState));
    res.end();
  }
});

app.listen(PORT, function () {
  return console.log('Listening on port ' + PORT);
});

/***/ })
/******/ ]);